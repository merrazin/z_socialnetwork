package ZSocialNetwork

import (
	srv "ZSocialNetwork/environment"
	types "ZSocialNetwork/types"
	userAgent "ZSocialNetwork/userAgent"
	"fmt"
	"math"
	"math/rand"
	"sync"
	"time"
)

type Simulation struct {
	Id_simulation string
	Users         []*userAgent.UserAgentUser
	Server        *srv.ServerAgent
	StartTime     time.Time
	maxDuration   time.Duration
	Content       map[string]string
	syncChans     sync.Map
	step          int
}

type ActionProbabilityVector struct {
	PostProbability     float64
	CommentProbability  float64
	LikeProbability     float64
	UnLikeProbability   float64
	FollowProbability   float64
	UnfollowProbability float64
	ShareProbability    float64
}

func NormalRand(mu, variance float64) float64 {
	// Box-Muller transform to generate standard normal distribution
	u1 := 1.0 - rand.Float64()
	u2 := 1.0 - rand.Float64()
	z := math.Sqrt(-2.0*math.Log(u1)) * math.Cos(2.0*math.Pi*u2)

	// Adjust for mean and variance
	val := mu + math.Sqrt(variance)*z
	if val > 1 {
		val = 1
	}
	if val < 0 {
		val = 0
	}
	return val
}
func InvertedNormalSample(mean, sd float64) float64 {
	// Generate standard normal distribution
	u1 := 1.0 - rand.Float64()
	u2 := 1.0 - rand.Float64()
	z := math.Sqrt(-2.0*math.Log(u1)) * math.Cos(2.0*math.Pi*u2)

	// Adjust for mean and standard deviation
	val := mean + sd*z

	// Clip the value to be within the range [0, 1]
	val = math.Min(1, math.Max(0, val))

	// Invert the distribution based on the distance from the center (0.5)
	distance := math.Abs(val - 0.5)
	if val < 0.5 {
		val = 0 + distance
	} else {
		// Ensure that the distance is not negative
		val = 1 - distance
		if val < 0 {
			val = 0
		}
	}

	return val
}

func generateProbabilities(x int) []float64 {
	probabilities := make([]float64, x)
	segment := 1.0 / float64(x)

	for i := 0; i < x-1; i++ {
		prob := rand.Float64() * segment
		probabilities[i] = prob
	}

	lastProb := 0.0
	for _, p := range probabilities {
		lastProb += p
	}
	probabilities[x-1] = 1.0 - lastProb

	return probabilities
}

func NewSimulation(Id_simulation string, maxDuration time.Duration, config types.Simulation_config) *Simulation {
	simu := &Simulation{}
	simu.Id_simulation = Id_simulation
	simu.Users = make([]*userAgent.UserAgentUser, 0)
	simu.Server = srv.NewServerAgent("server1")

	simu.maxDuration = maxDuration

	for i := 0; i < config.Number_Agents; i++ {
		// à cette étape, on peut modfier les probabilités d'actions des agents
		// + les prior opinions

		id := fmt.Sprintf("Agent #%d", i)
		var priorOpinion float64
		if config.Initial_Distribution == "uniform" {
			priorOpinion = rand.Float64() // loi uniforme entre 0 et 1
		} else if config.Initial_Distribution == "normal" {
			priorOpinion = NormalRand(0.5, 0.05)
		} else if config.Initial_Distribution == "inverted_normal" {
			priorOpinion = InvertedNormalSample(0.5, 0.2)
		} else {
			priorOpinion = NormalRand(0.5, 0.05)
		}

		syncChan := make(chan int)

		// prob := generateProbabilities(7)
		// rand.Shuffle(len(prob), func(i, j int) {
		// 	prob[i], prob[j] = prob[j], prob[i]
		// })

		prob := []float64{0.15, 0.2, 0.15, 0.1, 0.1, 0.1, .2}

		ActionProbabilityVector := ActionProbabilityVector{PostProbability: prob[0], CommentProbability: prob[1],
			LikeProbability: prob[2], UnLikeProbability: prob[3], FollowProbability: prob[4],
			UnfollowProbability: prob[5], ShareProbability: prob[6]}

		// choisir le type d'agent
		var typeAgent string
		randValue := rand.Float64()
		if randValue < config.Percentage_rational {
			typeAgent = "rational"
		} else if randValue < config.Percentage_troll+config.Percentage_rational {
			typeAgent = "troll"
		} else if randValue < config.Percentage_irrational+config.Percentage_troll+config.Percentage_rational {
			typeAgent = "irrational"
		} else {
			typeAgent = "rational"
			fmt.Println("Error in the percentage of agents. the rest of agents are set to rational")
		}

		ag := userAgent.NewUserAgentUser(id, id, priorOpinion, syncChan, simu.Server, typeAgent,
			ActionProbabilityVector.PostProbability, ActionProbabilityVector.CommentProbability,
			ActionProbabilityVector.LikeProbability, ActionProbabilityVector.UnLikeProbability,
			ActionProbabilityVector.FollowProbability, ActionProbabilityVector.UnfollowProbability,
			ActionProbabilityVector.ShareProbability)

		// ajout de l'agent à la simulation
		simu.Users = append(simu.Users, ag)

		// ajout de l'ajout à l'environnement

		newUserData := types.New_user{}
		newUserData.Id_user = id
		newUserData.Pseudo = id

		newUserData.PriorOpinion = priorOpinion

		simu.Server.CreateUser(newUserData)
		//fmt.Println(simu.Server)
		simu.syncChans.Store(id, syncChan)
	}

	return simu
}

func (simu *Simulation) Run() {
	simu.StartTime = time.Now()
	fmt.Println("Simulation started")
	for _, agt := range simu.Users {
		//gt_tmp := agt
		//agt_tmp.Start()
		func(agt *userAgent.UserAgentUser) {
			agt.Start()
		}(agt)
	}
	for _, agt := range simu.Users {
		fmt.Println("Agent ", agt.Id_user, " is playing")
		go func(agt *userAgent.UserAgentUser) {
			step := 0
			for {
				step++
				c, _ := simu.syncChans.Load(agt.Id_user)
				c.(chan int) <- step
				cooldown := rand.Intn(10)                         // /!\ utilisation d'un "Type Assertion"
				time.Sleep(time.Duration(cooldown) * time.Second) // "cool down"
				//time.Sleep(time.Duration(0) * time.Second) // no cool down
				<-c.(chan int)
			}
		}(agt)
	}
	//fmt.Println("Simulation finished")
	// for {
	// 	if time.Since(simu.StartTime) > simu.maxDuration {
	// 		break
	// 	}
	// }
}

/*
Rcode to generate random numbers with GRAPHS
vals<-rnorm(100000,0.5,0.2)
vals <- pmin(pmax(vals, 0), 1)
hist(vals,main="Normal distribution")


twodistro<-function(n,i,j){
  alpha<-sample(c(0,1),n,replace=TRUE)
  vect=alpha*rbeta(n,i,j)+(1-alpha)*rbeta(n,j,i)
  return (vect)
}
result=twodistro(10000,1,4)
hist(result, main = "Mixture of Beta Distributions", xlab = "Values", col = "lightblue", border = "black")


hist(runif(10000,min=0,max=1),main="Uniform distribution")
*/
