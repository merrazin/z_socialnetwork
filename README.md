# Projet Z_SocialNetwork

## Introduction

Bienvenue dans le projet ZSocialNetwork, une simulation avancée et interactive d'un réseau social politique. Ce projet, développé en Go, vise à modéliser et analyser les dynamiques complexes des interactions sociales en ligne. Il repose également sur la simulation d'agents utilisateurs, chacun doté de son ensemble de comportements et probabilités d'action tels que publier, commenter, aimer, suivre et partager du contenu.

## Étapes de lancement

Pour commencer à utiliser notre ZSocialNetwork, veuillez suivre les étapes ci-dessous :

### Clonage du dépôt

```bash
git clone https://gitlab.utc.fr/merrazin/z_socialnetwork.git
cd z_socialnetwork
```

### Modification du fichier de configuration

Configurer les différentes entrées de la simulation avec notre fichier de configuration :

- Number_Agents : Nombre d'agents que vous souhaitez avoir durant la simulation.
- Initial_Distribution : Distribution initiale des différentes opinions des différents agents. (Choix possibles : "normal", "uniform" et "inverted_normal")
- Percentage_rational : Pourcentage d'agents ayant des actions réalisées sur des posts et users proches de leurs opinions.
- Percentage_irrational : Pourcentage d'agents ayant des actions réalisées sur des posts et users aléatoires.
- Percentage_troll : Pourcentage d'agents ayant des actions réalisées sur des posts et users qui ne sont pas proches de leurs opinions.

Note : La somme de Percentage_rational, Percentage_irrational et Percentage_troll doit être égale à 1 sinon les probabilités supérieures à 1 ne seront pas considérées dans l'ordre donné. 

Exemple : 

- Percentage_rational : 0.5,
- Percentage_irrational : 0.3,
- Percentage_troll : 0.1,

Nous avons en somme 0.9 donc nous allons avoir automatiquement 0.6 en Percentage_rational, 0.3 en Percentage_irrational et 0.1 en Percentage_troll.

```
{

    "Number_Agents" : 600,

    "Initial_Distribution" : "normal",

    "Percentage_rational" : 0.95,

    "Percentage_irrational" : 0.05,

    "Percentage_troll": 0.0

}
```

### Exécution

Vous pouvez maintenant passer à l'exécution du projet. Celle-ci peut se faire de trois manières :

#### Soit vous pouvez utiliser l'exécutable déjà présent dans le dépôt git et prêt à l'emploi

```bash
./main
```

#### Soit vous pouvez créer un nouvel exécutable puis le lancer

```bash
go build cmd/main.go
./main
```

#### Soit vous pouvez exécuter directement le code source

```bash
go run cmd/main.go
```

### Affichage de notre simulation

- Notre graphe social avec la possibilité d'accéder aux comptes des utilisateurs en cliquant sur leur bulles

```
http://localhost:8080/users
```

- Nos différents graphiques

```
http://localhost:8080/dashboard
```

## Captures d'écran du projet

### Graphe social

Le graphe social de ZSocialNetwork est une représentation interactive et visuelle des connexions entre les utilisateurs. Chaque utilisateur est représenté par un nœud sur le graphe, avec plusieurs attributs distincts.

#### Couleur du Nœud

La couleur d'un nœud peut tendre soit vers le bleu, soit vers le rouge :
- une tendance vers le **bleu** signifie que l'opinion de l'utilisateur tend vers 0, indiquant une inclination vers l'extrême gauche du spectre politique ;
- une tendance vers le **rouge** signifie que l'opinion de l'utilisateur tend vers 1, indiquant une inclination vers l'extrême droite du spectre politique.

#### Taille du Nœud

La taille d'un nœud est proportionnelle au nombre d'abonnés de l'utilisateur. Plus un utilisateur a d'abonnés, plus son nœud est grand, reflétant son influence et sa popularité.

#### Arcs du Nœud

Les arcs entre les nœuds représentent les abonnements. Un arc partant d'un nœud i à un nœud j signifie que l'utilisateur i est abonné à l'utilisateur j.

#### Interactivité

Le graphe est interactif : il est possible de déplacer les nœuds et d'accéder aux profils des utilisateurs en cliquant sur eux.

Voici une capture d'écran d'un exemple de graphe social prise à un instant t d'une simulation :

![Graphe](Images/Graphe.png)

### Profil d'un utilisateur

Le profil des utilisateurs sur ZSocialNetwork offre un aperçu détaillé de leurs activités et de leurs interactions au sein du réseau. Au sein d'un profil, une opinion est de couleur bleue si elle est comprise entre 0 et 0.5, ou rouge si elle est comprise entre 0.5 et 1. Un profil présente plusieurs éléments clés.

#### Informations de l'utilisateur

- **Évolution de l'opinion** : L'évolution de l'opinion de l'utilisateur est visible dans le coin supérieur droit du profil. Cette section met en évidence le changement de l'opinion, présenté sous la forme "Opinion primitive -> Nouvelle opinion estimée par le serveur".
- **Abonnés** : Les personnes abonnées à l'utilisateur.
- **Abonnements** : Les personnes auxquelles l'utilisateur s'est abonné.

#### Fil d'actualités

Le fil d'actualités regroupe des posts et des commentaires de post, tous accompagnés d'une opinion. Les posts et leurs commentaires sont représentés du plus récent au plus ancien. Un post peut être liké et partagé par des utilisateurs.

Voici des captures d'écran d'un exemple de profil d'utilisateur sur ZSocialNetwork :

Capture du profil n°1 :

![Profil1](Images/Profil1.png)

Capture du profil n°2 :

![Profil2](Images/Profil2.png)

Capture du profil n°3 :

![Profil3](Images/Profil3.png)

Capture du profil n°4 :

![Profil4](Images/Profil4.png)

### Tableau de bord

Le tableau de bord présente des statistiques globales et des analyses de tendances du réseau.

#### Graphique représentant l'évolution, au cours d'une simulation, des erreurs absolues moyennes entre les opinions primitives et les opinions actualisées des utilisateurs

![Graphique](Images/Graphique.png)

#### Histogramme des opinions primitives des utilisateurs

![HistogrammeOpinionsPrimitives](Images/HistogrammeOpinionsPrimitives.png)

#### Histogramme des opinions actuelles des utilisateurs à un instant t d'une simulation

![HistogrammeOpinionsActuelles](Images/HistogrammeOpinionsActuelles.png)

#### Classement des dix utilisateurs les plus suivis à un instant t d'une simulation

![Classement](Images/Classement.png)

#### Diagramme Donut illustrant le rapport entre le nombre d'utilisateurs et le nombre de posts à un instant t d'une simulation

![Diagramme](Images/Diagramme.png)

## Conclusion

Vous avez maintenant toutes les étapes pour explorer notre projet de simulation d'un réseau social politique . Nous avons effectué quelques améliorations. En effet,depuis la présentation de jeudi, nous avons fait des modifications afin d'optimiser les performances et s'approcher d'un réseau social réel.

- Visualisation améliorée avec 4 000 Agents : L'augmentation du nombre d'agents à 4 000 a permis une visualisation plus claire des "Small worlds" sur le graphe social. Cette amélioration apporte une perspective plus riche sur la façon dont les utilisateurs interagissent et forment des groupes interconnectés au sein du réseau.

- Optimisation de la mise à Jour du FrontEnd : Les performances du front end ont été améliorées, réduisant le temps de mise à jour à 0.7 seconde, contre 1.2 seconde lors de notre présentation. En effet, durant cette dernière , des problèmes liés au cache du navigateur avaient empêché l'affichage en temps réel des changements sur le front end.

Voici une capture d'écran d'un graphe social généré à partir de 4 000 agents :

![NouveauGraphe](Images/NouveauGraphe.png)

Notons qu'il est préférable de diminuer le zoom du navigateur autour de 40% pour avoir une bonne vue d'ensemble du graphe.