package main

import (
	api "ZSocialNetwork/api"
	simulation "ZSocialNetwork/simulation"
	types "ZSocialNetwork/types"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

func main() {
	logFile, err := os.Create("logfile.txt")
	if err != nil {
		log.Fatal(err)
	}
	defer logFile.Close()
	log.SetOutput(logFile)

	// Read the JSON configuration from file
	configFile, err := ioutil.ReadFile("./ConfigSimu/configSimu.json")
	if err != nil {
		log.Fatal("Error reading config file:", err)
	}

	// Unmarshal the JSON data into the Simulation_config struct
	var simu_config types.Simulation_config
	err = json.Unmarshal(configFile, &simu_config)
	if err != nil {
		log.Fatal("Error unmarshalling JSON:", err)
	}
	fmt.Println(simu_config)
	s := simulation.NewSimulation("simulation1", 12000*time.Second, simu_config)
	go api.StartAPI(s)
	s.Run()
	time.Sleep(200 * time.Minute)
	print("fin")
}
