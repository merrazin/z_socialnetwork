// displayData.js
document.addEventListener("DOMContentLoaded", function () {
    const apiUrl = "http://localhost:8080/dataDashboard";



    const gaugeChartCtx = document.getElementById("GaugeChart").getContext("2d");
    const gaugeChart = new Chart(gaugeChartCtx, {
        type: 'doughnut',
        data: {
            labels: ['Number of Agents', 'Number of Posts'],
            datasets: [{
                data: [/* Number of Agents, Number of Posts */],
                backgroundColor: ['#36A2EB', '#FFCE56'], // Adjust colors as needed
                borderWidth: 2, // Adjust the border width
            }],
        },
        options: {
            responsive: true,
            legend: {
                display: true,
                position: 'bottom',
            },
            tooltips: {
                enabled: true,
            },
        },
    });
    
    
    
    const MAEctx = document.getElementById("MAEChart").getContext("2d");
    // Initialize the Mean Absolute Error chart
    const MAEChart = new Chart(MAEctx, {
      type: 'line',
      data: {
        labels: [],
        datasets: [{
          label: 'Mean Absolute Error over Time',
          borderColor: 'red',
          data: [],
          spanGaps: true, // Enable progressive animation
            tension: 0.2, // Adjust the tension for smoother curves
        }],
      },
      options: {
        scales: {
          x: [{
            type: 'linear',
            position: 'bottom',
          }],
          y: [{
            type: 'linear',
            position: 'left',
          }],
        },
      },
    });
    
  
    // Initialize the histogram chart

    // Histogram Chart
    const histogramCtx = document.getElementById("histogramChart").getContext("2d");
    const histogramChart = new Chart(histogramCtx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Opinion Distribution',
                backgroundColor: 'blue',
                data: [],
            }],
        },
        options: {
            scales: {
                x: [{
                    type: 'linear',
                    position: 'bottom',
                }],
                y: [{
                    type: 'linear',
                    position: 'left',
                }],
            },
        },
    });



        // Histogram Chart
        const HistogramPriorOpinion = document.getElementById("HistogramPriorOpinion").getContext("2d");
        const HistogramPriorOpinionChart = new Chart(HistogramPriorOpinion, {
            type: 'bar',
            data: {
                labels: [],
                datasets: [{
                    label: 'Opinion Distribution',
                    backgroundColor: 'blue',
                    data: [],
                }],
            },
            options: {
                scales: {
                    x: [{
                        type: 'linear',
                        position: 'bottom',
                    }],
                    y: [{
                        type: 'linear',
                        position: 'left',
                    }],
                },
            },
        });



    const TopFollowersCtx = document.getElementById("barChart").getContext("2d");
    const TopFollowersChart = new Chart(TopFollowersCtx, {
        type: 'bar',
        data: {
            labels: [],
            datasets: [{
                label: 'Followers Count',
                data: [],
                backgroundColor: [], // Added line for colors
                borderWidth: 2, // Border width for each bar
            }],
        },
        options: {
            indexAxis: 'y', // Specify the horizontal axis
            responsive: true,
            plugins: {
                legend: {
                    position: 'right',
                },
                title: {
                    display: true,
                    text: 'Top Influencers Followers Count',
                },
            },
            scales: {
                x: {
                    beginAtZero: true,
                },
            },
        },
    });
    

    // Function to fetch data from the API and update the chart
    async function fetchData() {
      try {
        const response = await fetch(apiUrl);
        const data = await response.json();
        const time = data.TimeSimu;
        updateGaugeCharts(data.Count_users, data.Count_posts)
        updateChartMAEPLOT(data.MAE_Opinion, time);
        updateHistogramChart(data.AllOpinions_Histogram);
        updateTopFollowersChart(data.Top_Influencers);
        updateHistogramPriorOpinionChart(data.AllOpinions_Prior);

      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
    
    // Function to update the doughnut charts
    function updateGaugeCharts(numberOfAgents, numberOfPosts) {
        // Update data in the chart
        gaugeChart.data.datasets[0].data = [numberOfAgents, numberOfPosts];
    
        // Update the chart
        gaugeChart.update();
    }

    // Function to update the chart data
    function updateChartMAEPLOT(seValue, time) {

        MAEChart.data.labels.push(time);
        MAEChart.data.datasets[0].data.push(seValue);  
      // Update the chart
      // Remove the oldest data point if the chart exceeds a certain number of points
      MAEChart.update();
    }

    function updateHistogramChart(histogramData) {

      histogramChart.data.labels = [];
      histogramChart.data.datasets[0].data = [];
      histogramChart.data.datasets[0].backgroundColor = []; // Added line for colors

      // Number of bins(les barres) for the histogram
      const numBins = 10;
      const binWidth = 1 / numBins;

      // Initialize bins
      const bins = Array.from({ length: numBins }, (_, index) => index * binWidth);

      // Count values in each bin
      const counts = Array(numBins).fill(0);
      for (const value of histogramData) {
          const binIndex = Math.floor(value / binWidth);
          counts[binIndex]++;
          
      }

      // Update chart data
      for (let i = 0; i < numBins; i++) {
          const binLabel = `${(i * binWidth).toFixed(1)}-${((i + 1) * binWidth).toFixed(1)}`;
          histogramChart.data.labels.push(binLabel);
          histogramChart.data.datasets[0].data.push(counts[i]); 

          
          const normalizedX = i / (numBins - 1); // Normalize to [0, 1]
          const color = `rgba(${Math.round(255 * (normalizedX))}, 0, ${Math.round(255 * (1-normalizedX))}, 0.8)`;
          histogramChart.data.datasets[0].backgroundColor.push(color);
      }

      histogramChart.update();
  }

  /// Update the histogram of the prior opinion
  function updateHistogramPriorOpinionChart(histogramPriorOpinionData) {

    HistogramPriorOpinionChart.data.labels = [];
    HistogramPriorOpinionChart.data.datasets[0].data = [];
    HistogramPriorOpinionChart.data.datasets[0].backgroundColor = []; // Added line for colors

    // Number of bins(les barres) for the histogram
    const numBins = 10;
    const binWidth = 1 / numBins;

    // Initialize bins
    const bins = Array.from({ length: numBins }, (_, index) => index * binWidth);

    // Count values in each bin
    const counts = Array(numBins).fill(0);
    for (const value of histogramPriorOpinionData) {
        const binIndex = Math.floor(value / binWidth);
        counts[binIndex]++;
        
    }

    // Update chart data
    for (let i = 0; i < numBins; i++) {
        const binLabel = `${(i * binWidth).toFixed(1)}-${((i + 1) * binWidth).toFixed(1)}`;
        HistogramPriorOpinionChart.data.labels.push(binLabel);
        HistogramPriorOpinionChart.data.datasets[0].data.push(counts[i]); 

        
        const normalizedX = i / (numBins - 1); // Normalize to [0, 1]
        const color = `rgba(${Math.round(255 * (normalizedX))}, 0, ${Math.round(255 * (1-normalizedX))}, 0.8)`;
        HistogramPriorOpinionChart.data.datasets[0].backgroundColor.push(color);
    }

    HistogramPriorOpinionChart.update();
}
function updateTopFollowersChart(topInfluencers) {
  // Clear previous data
  TopFollowersChart.data.labels = [];
  TopFollowersChart.data.datasets[0].data = [];
  TopFollowersChart.data.datasets[0].backgroundColor = []; // Added line for colors

  // // Update chart data
  for (let i = 0; i < topInfluencers.Opinion_List.length; i++) {
      const nameAndOpinion = `${topInfluencers.IdUser_List[i]} (${topInfluencers.Opinion_List[i].toFixed(2)})`;
      TopFollowersChart.data.labels.push(nameAndOpinion); 
      TopFollowersChart.data.datasets[0].data.push(topInfluencers.Count_Followers[i]);

      // Choose color based on opinion
      const opinion = topInfluencers.Opinion_List[i];
      const color = `rgba(${Math.round(255 * (opinion))}, 0, ${Math.round(255 * (1-opinion))}, 0.8)`;
      TopFollowersChart.data.datasets[0].backgroundColor.push(color);
  }
  TopFollowersChart.update();
}

    // Fetch data periodically
    setInterval(fetchData, 600); // 1.005 seconds
  });
  