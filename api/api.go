package ZSocialNetwork

import (
	simulation "ZSocialNetwork/simulation"
	types "ZSocialNetwork/types"
	"encoding/json"
	"fmt"
	"log"
	"math"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"text/template"
	"time"
)

func userExist(userId string, users []types.GraphDataUser) bool {

	res := false
	taille := len(users)

	for i := 0; i < taille && !res; i++ {
		res = users[i].Id_User == userId
	}

	return res
}

func formatDate(date string) string {

	res, _ := time.Parse(time.RFC3339, date)
	formattedDate := res.Format("2 January 2006 à 15:04")
	month := res.Month().String()
	months := map[string]string{
		"January":   "Janvier",
		"February":  "Février",
		"March":     "Mars",
		"April":     "Avril",
		"May":       "Mai",
		"June":      "Juin",
		"July":      "Juillet",
		"August":    "Août",
		"September": "Septembre",
		"October":   "Octobre",
		"November":  "Novembre",
		"December":  "Décembre",
	}

	return strings.Replace(formattedDate, month, months[month], 1)
}

func getSortedUsersKeys(users map[string]bool) []string {

	res := make([]string, 0)

	for i := range users {
		if users[i] {
			res = append(res, i)
		}
	}
	sort.Strings(res)

	return res
}

func getSortedCommentsKeys(comments map[string]types.CommentData) []string {

	res := make([]string, 0)

	for i := range comments {
		res = append(res, i)
	}
	sort.Slice(res, func(i, j int) bool {
		timeI, _ := time.Parse(time.RFC3339, comments[res[i]].Date_comment)
		timeJ, _ := time.Parse(time.RFC3339, comments[res[j]].Date_comment)
		return timeI.After(timeJ)
	})

	return res
}

func StartAPI(sim *simulation.Simulation) {
	mux := http.NewServeMux()

	DataDashboardFunc := func(w http.ResponseWriter, r *http.Request) {
		msg, _ := json.Marshal(sim.Server.MonitoringData(sim.StartTime))
		fmt.Fprintf(w, "%s", msg)
	}
	mux.HandleFunc("/dataDashboard", DataDashboardFunc)

	mux.HandleFunc("/usersJSON", func(w http.ResponseWriter, r *http.Request) {
		msg, _ := json.Marshal(sim.Server.GraphData())
		fmt.Fprintf(w, "%s", msg)
	})

	mux.HandleFunc("/profileJSON", func(w http.ResponseWriter, r *http.Request) {
		userId := r.URL.Query().Get("userId")
		msg, _ := json.Marshal(sim.Server.GetUserFULLDataFrontEnd(userId))
		fmt.Fprintf(w, "%s", msg)
	})

	mux.HandleFunc("/dashboardJS.js", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "api/dashboardJS.js")
	})
	mux.HandleFunc("/dashboard", func(w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.ParseFiles("api/dashboard.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		tmpl.Execute(w, nil)
	})

	mux.HandleFunc("/users", func(w http.ResponseWriter, r *http.Request) {
		users, _ := json.Marshal(sim.Server.GraphData().List_IdUser)
		tmplData := map[string]interface{}{
			"Users": string(users),
		}
		tmpl, err := template.ParseFiles("api/SocialGraph.html")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		err = tmpl.Execute(w, tmplData)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	})

	mux.HandleFunc("/profile", func(w http.ResponseWriter, r *http.Request) {
		if userId := r.URL.Query().Get("userId"); len(userId) > 0 && userExist(userId, sim.Server.GraphData().List_IdUser) {
			user := sim.Server.GetUserFULLDataFrontEnd(userId)
			user.PriorOpinion = math.Round(user.PriorOpinion*100) / 100
			user.Opinion = math.Round(user.Opinion*100) / 100
			sort.Strings(user.List_Followers)
			sort.Strings(user.List_Following)
			sort.Slice(user.Feed, func(i, j int) bool {
				timeI, _ := time.Parse(time.RFC3339, user.Feed[i].Date_post)
				timeJ, _ := time.Parse(time.RFC3339, user.Feed[j].Date_post)
				return timeI.After(timeJ)
			})
			tmplData := map[string]interface{}{
				"User": user,
			}
			tmpl, err := template.New("UserProfile.html").Funcs(template.FuncMap{
				"encodeURIComponent":    url.QueryEscape,
				"formatDate":            formatDate,
				"getSortedUsersKeys":    getSortedUsersKeys,
				"getSortedCommentsKeys": getSortedCommentsKeys}).ParseFiles("api/UserProfile.html")
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			err = tmpl.Execute(w, tmplData)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
		} else {
			http.Redirect(w, r, "/users", http.StatusFound)
		}
	})

	s := &http.Server{
		Addr:           ":8080",
		Handler:        mux,
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20}
	log.Println("Listening on localhost:8080")
	log.Fatal(s.ListenAndServe())
}
