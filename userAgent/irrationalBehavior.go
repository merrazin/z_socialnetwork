package ZSocialNetwork

import (
	"fmt"
	"math/rand"
)


type IrrationalBehavior struct{}

func (i *IrrationalBehavior) Deliberate(user *UserAgentUser) {
	proba_action := rand.Float64()

	user.CurrentDecision.ActionName = "Nothing"
	if proba_action < user.p_post {
		user.CurrentDecision.ActionName = "Post"

	} else if proba_action < user.p_post+user.p_like {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}

		// find list of posts that are not liked
		var list_posts_not_liked []string
		for _, post := range user.FeedList {
			// test if the key(=post.Id_post) exists in the map
			_, findMap := post.List_id_likers[user.Id_user]
			if !findMap {
				list_posts_not_liked = append(list_posts_not_liked, post.Id_post)
			}
		}
		if len(list_posts_not_liked) == 0 {
			return
		}

		// sort posts by distance

		// for irrational agents, we choose randomly
		if user.typeBehaviour == "irrational" {
			index := rand.Intn(len(list_posts_not_liked))
			user.CurrentDecision.PostID = list_posts_not_liked[index]
		}

		user.CurrentDecision.ActionName = "Like"

	} else if proba_action < user.p_post+user.p_like+user.p_comment {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}

		// find list of posts that I have not commented
		var list_posts_not_commented []string
		for _, post := range user.FeedList {
			commented := false
			for _, value := range post.List_comments {
				if value.Id_author == user.Id_user {
					commented = true
					break
				}
			}
			if !commented {
				list_posts_not_commented = append(list_posts_not_commented, post.Id_post)
			}
		}
		if len(list_posts_not_commented) == 0 {
			return
		}

		// for rational agents, we choose the post that is the closest to my opinion

		// for irrational agents, we choose randomly
		index := rand.Intn(len(list_posts_not_commented))
		user.CurrentDecision.PostID = list_posts_not_commented[index]

		// for troll agents, we choose the post that is the farthest to my opinion

		user.CurrentDecision.ActionName = "Comment"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}

		// find list of posts that I have liked
		var list_posts_liked []string
		for _, post := range user.FeedList {
			// test if the key(=post.Id_post) exists in the map
			_, findMap := post.List_id_likers[user.Id_user]
			if findMap {
				list_posts_liked = append(list_posts_liked, post.Id_post)
			}
		}
		if len(list_posts_liked) == 0 {
			return
		}

		// sort posts by distance

		// for rational agents, we choose to unlike a post that is furthest to my opinion

		if user.typeBehaviour == "irrational" {
			// for irrational agents, we choose randomly
			index := rand.Intn(len(list_posts_liked))
			user.CurrentDecision.PostID = list_posts_liked[index]
		}

		user.CurrentDecision.ActionName = "RemoveLike"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}
		// find a list of users that I have not followed

		var list_users_not_followed_withPriorOpinion []KeyValue

		for _, post := range user.FeedList {
			_, didIFollow := user.userDataInServer.List_Following[post.Id_author]
			if !didIFollow && post.Id_author != user.Id_user {
				list_users_not_followed_withPriorOpinion = append(list_users_not_followed_withPriorOpinion, KeyValue{post.Id_author, post.PriorOpinion})
			}
		}
		if len(list_users_not_followed_withPriorOpinion) == 0 {
			return
		}

		// sort following users by distance

		if user.typeBehaviour == "irrational" {
			// for irrational agents, we choose randomly
			index := rand.Intn(len(list_users_not_followed_withPriorOpinion))
			user.CurrentDecision.OtherUserID = list_users_not_followed_withPriorOpinion[index].Key
		}

		user.CurrentDecision.ActionName = "Follow"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow+user.p_unfollow {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}
		// find a list of users that I have followed
		var list_users_not_followed_withPriorOpinion []KeyValue
		for _, post := range user.FeedList {
			_, didIFollow := user.userDataInServer.List_Following[post.Id_author]
			if didIFollow {
				list_users_not_followed_withPriorOpinion = append(list_users_not_followed_withPriorOpinion, KeyValue{post.Id_author, post.PriorOpinion})
			}
		}
		if len(list_users_not_followed_withPriorOpinion) == 0 {
			return
		}

		// sort following users by distance

		if user.typeBehaviour == "irrational" {
			// for irrational agents, we choose randomly
			index := rand.Intn(len(list_users_not_followed_withPriorOpinion))
			user.CurrentDecision.OtherUserID = list_users_not_followed_withPriorOpinion[index].Key
		}
		user.CurrentDecision.ActionName = "Unfollow"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow+user.p_unfollow+user.p_share {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}
		// find a list of posts that I have not shared
		var list_posts_not_shared []string
		for _, post := range user.FeedList {
			_, didIShare := user.userDataInServer.List_sharedpost[post.Id_post]
			if !didIShare && post.Id_author != user.Id_user {
				list_posts_not_shared = append(list_posts_not_shared, post.Id_post)
			}
		}

		if len(list_posts_not_shared) == 0 {
			return
		}

		// sort posts by distance

		if user.typeBehaviour == "irrational" {
			index := rand.Intn(len(list_posts_not_shared))
			user.CurrentDecision.PostID = list_posts_not_shared[index]
		}

		user.CurrentDecision.ActionName = "Share"

	} else {

		fmt.Println("Are u stupid? the sum of probabilities is not equal to 1")

	}
}