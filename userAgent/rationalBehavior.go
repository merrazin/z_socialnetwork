package ZSocialNetwork

import (
	"fmt"
	"math/rand"
)


//////////////////////////////////////////////
type RationalBehavior struct{}

func (r *RationalBehavior) Deliberate(user *UserAgentUser) {
	proba_action := rand.Float64()

	user.CurrentDecision.ActionName = "Nothing"
	if proba_action < user.p_post {
		user.CurrentDecision.ActionName = "Post"

	} else if proba_action < user.p_post+user.p_like {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}

		// find list of posts that are not liked
		var list_posts_not_liked []string
		for _, post := range user.FeedList {
			// test if the key(=post.Id_post) exists in the map
			_, findMap := post.List_id_likers[user.Id_user]
			if !findMap {
				list_posts_not_liked = append(list_posts_not_liked, post.Id_post)
			}
		}
		if len(list_posts_not_liked) == 0 {
			return
		}

		// sort posts by distance

		keyValStruct := user.filterPostsByDistanceRanked(list_posts_not_liked, user.PriorOpinion)

		if user.typeBehaviour == "rational" {
			// for rational agents, we choose the post that is the closest to my opinion
			if len(list_posts_not_liked) > 4 {
				user.CurrentDecision.PostID = keyValStruct[rand.Intn(4)].Key
			} else {
				user.CurrentDecision.PostID = keyValStruct[0].Key
			}
		}

		user.CurrentDecision.ActionName = "Like"

	} else if proba_action < user.p_post+user.p_like+user.p_comment {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}

		// find list of posts that I have not commented
		var list_posts_not_commented []string
		for _, post := range user.FeedList {
			commented := false
			for _, value := range post.List_comments {
				if value.Id_author == user.Id_user {
					commented = true
					break
				}
			}
			if !commented {
				list_posts_not_commented = append(list_posts_not_commented, post.Id_post)
			}
		}
		if len(list_posts_not_commented) == 0 {
			return
		}

		// for rational agents, we choose the post that is the closest to my opinion

		// for irrational agents, we choose randomly
		index := rand.Intn(len(list_posts_not_commented))
		user.CurrentDecision.PostID = list_posts_not_commented[index]

		// for troll agents, we choose the post that is the farthest to my opinion

		user.CurrentDecision.ActionName = "Comment"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}

		// find list of posts that I have liked
		var list_posts_liked []string
		for _, post := range user.FeedList {
			// test if the key(=post.Id_post) exists in the map
			_, findMap := post.List_id_likers[user.Id_user]
			if findMap {
				list_posts_liked = append(list_posts_liked, post.Id_post)
			}
		}
		if len(list_posts_liked) == 0 {
			return
		}

		// sort posts by distance

		keyValStruct := user.filterPostsByDistanceRanked(list_posts_liked, user.PriorOpinion)

		// for rational agents, we choose to unlike a post that is furthest to my opinion

		if user.typeBehaviour == "rational" {
			user.CurrentDecision.PostID = keyValStruct[len(keyValStruct)-1].Key
		}

		// for irrational agents, we choose randomly
		//index := rand.Intn(len(list_posts_liked))
		//user.CurrentDecision.PostID = list_posts_liked[index]

		// for troll agents, we choose to unlike a post that is closest to my opinion
		//user.CurrentDecision.PostID = keyValStruct[0].Key

		user.CurrentDecision.ActionName = "RemoveLike"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}
		// find a list of users that I have not followed

		var list_users_not_followed_withPriorOpinion []KeyValue

		for _, post := range user.FeedList {
			_, didIFollow := user.userDataInServer.List_Following[post.Id_author]
			if !didIFollow && post.Id_author != user.Id_user {
				list_users_not_followed_withPriorOpinion = append(list_users_not_followed_withPriorOpinion, KeyValue{post.Id_author, post.PriorOpinion})
			}
		}
		if len(list_users_not_followed_withPriorOpinion) == 0 {
			return
		}

		// sort following users by distance
		keyValStruct := user.filterFollowingByDistanceRanked(list_users_not_followed_withPriorOpinion, user.PriorOpinion)

		if user.typeBehaviour == "rational" {
			// for rational agents, we choose to follow a user that is closest to my opinion
			user.CurrentDecision.OtherUserID = keyValStruct[0].Key
		}

		// for rational agents, we choose to follow a user that is closest to my opinion
		//user.CurrentDecision.OtherUserID = keyValStruct[0].Key
		//fmt.Println("I " + user.Id_user + " have just followed" + user.CurrentDecision.OtherUserID)
		// for irrational agents, we choose randomly
		//index := rand.Intn(len(list_users_not_followed))
		//user.CurrentDecision.OtherUserID = list_users_not_followed[index]

		// for troll agents, we choose to follow a user that is furthest to my opinion
		//user.CurrentDecision.OtherUserID = keyValStruct[len(keyValStruct)-1].Key

		user.CurrentDecision.ActionName = "Follow"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow+user.p_unfollow {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}
		// find a list of users that I have followed
		var list_users_not_followed_withPriorOpinion []KeyValue
		for _, post := range user.FeedList {
			_, didIFollow := user.userDataInServer.List_Following[post.Id_author]
			if didIFollow {
				list_users_not_followed_withPriorOpinion = append(list_users_not_followed_withPriorOpinion, KeyValue{post.Id_author, post.PriorOpinion})
			}
		}
		if len(list_users_not_followed_withPriorOpinion) == 0 {
			return
		}

		// sort following users by distance
		keyValStruct := user.filterFollowingByDistanceRanked(list_users_not_followed_withPriorOpinion, user.PriorOpinion)

		if user.typeBehaviour == "rational" {
			// for rational agents, we choose to unfollow a user that is furthest to my opinion
			user.CurrentDecision.OtherUserID = keyValStruct[len(keyValStruct)-1].Key
		}
		user.CurrentDecision.ActionName = "Unfollow"

	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow+user.p_unfollow+user.p_share {
		// test if feed is empty
		if len(user.FeedList) == 0 {
			return
		}
		// find a list of posts that I have not shared
		var list_posts_not_shared []string
		for _, post := range user.FeedList {
			_, didIShare := user.userDataInServer.List_sharedpost[post.Id_post]
			if !didIShare && post.Id_author != user.Id_user {
				list_posts_not_shared = append(list_posts_not_shared, post.Id_post)
			}
		}

		if len(list_posts_not_shared) == 0 {
			return
		}

		// sort posts by distance

		keyValStruct := user.filterPostsByDistanceRanked(list_posts_not_shared, user.PriorOpinion)

		if user.typeBehaviour == "rational" {
			// for rational agents, we choose to share a post that is closest to my opinion
			user.CurrentDecision.PostID = keyValStruct[0].Key
		}

		user.CurrentDecision.ActionName = "Share"

	} else {

		// in case the probabilities are not well defined (sum != 1)

		// if len(user.FeedList) == 0 {
		// 	user.CurrentDecision.ActionName = "Post"
		// 	user.CurrentDecision.Content = "content"
		// }
		fmt.Println("Are u stupid? the sum of probabilities is not equal to 1")

	}
}