package ZSocialNetwork

import (
	srv "ZSocialNetwork/environment"
	types "ZSocialNetwork/types"
	"bufio"
	"fmt"
	"log"
	"math"
	"math/rand"
	"os"
	"sort"
	"time"
)

// pour stocker les données de décision
type ActionData struct {
	ActionName  string // post & comment & follow & unfollow & like & unlike & share
	PostID      string // like & unlike & share
	Content     string // create post & New_comment
	OtherUserID string // follow & unfollow
	//Opinion     float64
}

type UserAgentUser struct {
	Id_user      string  `json:"id_user"`
	Pseudo       string  `json:"pseudo"`
	PriorOpinion float64 `json:"priorOpinion"`

	// reference to the environment

	syncChan chan int
	env      *srv.ServerAgent

	// data from the server that only concern the user

	userDataInServer types.UserAgent
	FeedList         []types.Post

	//

	CurrentDecision  ActionData
	typeBehaviour    string
	behaviorStrategy BehaviorStrategy
	// probabilities to take an action

	p_post     float64
	p_comment  float64
	p_like     float64
	p_unlike   float64
	p_follow   float64
	p_unfollow float64
	p_share    float64

	List_posts_shared map[string]bool
}

type KeyValue struct {
	Key   string
	Value float64
}

type BehaviorStrategy interface {
	Deliberate(user *UserAgentUser)
}

// ////////////////////////////////////////////
func testIfInSlice(slice []string, value string) bool {
	for _, v := range slice {
		if v == value {
			return true
		}
	}
	return false
}

func (ua *UserAgentUser) filterPostsByDistanceRanked(list_posts_ToChooseFrom []string, opinion float64) []KeyValue {
	posts_distance := make(map[string]float64)

	for _, post := range ua.FeedList {
		if testIfInSlice(list_posts_ToChooseFrom, post.Id_post) {
			posts_distance[post.Id_post] = math.Abs(post.PriorOpinion - opinion)
		}

	}
	// sort the map by value

	var sortedPosts []KeyValue
	for id, distance := range posts_distance {
		sortedPosts = append(sortedPosts, KeyValue{id, distance})
	}

	// Sort the slice by values
	sort.Slice(sortedPosts, func(i, j int) bool {
		return sortedPosts[i].Value < sortedPosts[j].Value
	})

	return sortedPosts
}

func (user *UserAgentUser) filterFollowingByDistanceRanked(list_users_ToChooseFrom []KeyValue, opinion float64) []KeyValue {
	users_distance := make(map[string]float64)

	for _, keyVal := range list_users_ToChooseFrom {

		users_distance[keyVal.Key] = math.Abs(keyVal.Value - opinion)
	}
	// sort the map by value

	var sortedUsers []KeyValue
	for id, distance := range users_distance {
		sortedUsers = append(sortedUsers, KeyValue{id, distance})
	}

	// Sort the slice by values
	sort.Slice(sortedUsers, func(i, j int) bool {
		return sortedUsers[i].Value < sortedUsers[j].Value
	})

	return sortedUsers
}

func (user *UserAgentUser) Start() {
	// log.Printf("%s starting...\n", user.Id_user)
	//user := user
	go func() {
		var step int
		for {
			step = <-user.syncChan
			//fmt.Println(user.Id_user)
			user.UpdateMyFeed(user.env)
			user.Percept(user.env)
			user.Deliberate()
			user.Act(user.env)
			user.syncChan <- step
		}
	}()

}

func NewUserAgentUser(Id_user string, Pseudo string, PriorOpinion float64,
	syncChan chan int, env *srv.ServerAgent, typeBehaviour string,
	p_post float64, p_comment float64, p_like float64,
	p_unlike float64, p_follow float64, p_unfollow float64,
	p_share float64) *UserAgentUser {

	var BehaviorStrategy BehaviorStrategy
	switch typeBehaviour {
	case "rational":
		BehaviorStrategy = &RationalBehavior{}
	case "troll":
		BehaviorStrategy = &TrollBehavior{}
	case "irrational":
		BehaviorStrategy = &IrrationalBehavior{}
	}

	return &UserAgentUser{Id_user, Pseudo, PriorOpinion,
		syncChan, env, types.UserAgent{},
		nil, ActionData{}, typeBehaviour, BehaviorStrategy,
		p_post, p_comment, p_like, p_unlike, p_follow, p_unfollow, p_share,
		nil}
}

func (user *UserAgentUser) GetRandomPost(opinion float64) {
	var filename string
	if opinion >= 0 && opinion < 0.1 {
		filename = "posts_0_to_01.txt"
	} else if opinion >= 0.1 && opinion < 0.2 {
		filename = "posts_01_to_02.txt"
	} else if opinion >= 0.2 && opinion < 0.3 {
		filename = "posts_02_to_03.txt"
	} else if opinion >= 0.3 && opinion < 0.4 {
		filename = "posts_03_to_04.txt"
	} else if opinion >= 0.4 && opinion < 0.5 {
		filename = "posts_04_to_05.txt"
	} else if opinion >= 0.5 && opinion < 0.6 {
		filename = "posts_05_to_06.txt"
	} else if opinion >= 0.6 && opinion < 0.7 {
		filename = "posts_06_to_07.txt"
	} else if opinion >= 0.7 && opinion < 0.8 {
		filename = "posts_07_to_08.txt"
	} else if opinion >= 0.8 && opinion < 0.9 {
		filename = "posts_08_to_09.txt"
	} else if opinion >= 0.9 && opinion <= 1 {
		filename = "posts_09_to_1.txt"
	}

	file, err := os.Open("content/" + filename)
	fileInfo, err := os.Stat("content/" + filename)
	if fileInfo.Size() == 0 {
		return
	}
	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	rand.Seed(time.Now().Unix())

	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	randomLineNumber := rand.Intn(len(lines))

	user.CurrentDecision.Content = lines[randomLineNumber]
	return

}

func (user *UserAgentUser) GetRandomComment(opinion float64) {
	var filename string
	if opinion >= 0 && opinion < 0.1 {
		filename = "comments_0_to_01.txt"
	} else if opinion >= 0.1 && opinion < 0.2 {
		filename = "comments_01_to_02.txt"
	} else if opinion >= 0.2 && opinion < 0.3 {
		filename = "comments_02_to_03.txt"
	} else if opinion >= 0.3 && opinion < 0.4 {
		filename = "comments_03_to_04.txt"
	} else if opinion >= 0.4 && opinion < 0.5 {
		filename = "comments_04_to_05.txt"
	} else if opinion >= 0.5 && opinion < 0.6 {
		filename = "comments_05_to_06.txt"
	} else if opinion >= 0.6 && opinion < 0.7 {
		filename = "comments_06_to_07.txt"
	} else if opinion >= 0.7 && opinion < 0.8 {
		filename = "comments_07_to_08.txt"
	} else if opinion >= 0.8 && opinion < 0.9 {
		filename = "comments_08_to_09.txt"
	} else if opinion >= 0.9 && opinion <= 1 {
		filename = "comments_09_to_1.txt"
	}

	file, err := os.Open("content/" + filename)
	fileInfo, err := os.Stat("content/" + filename)
	if fileInfo.Size() == 0 {
		return
	}
	defer file.Close()
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	rand.Seed(time.Now().Unix())

	var lines []string
	for scanner.Scan() {
		lines = append(lines, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	randomLineNumber := rand.Intn(len(lines))

	user.CurrentDecision.Content = lines[randomLineNumber]
	return

}

func (user *UserAgentUser) UpdateMyFeed(realServer *srv.ServerAgent) {
	realServer.UpdateUserFeed(user.Id_user)
}
func (user *UserAgentUser) Percept(copyServer *srv.ServerAgent) error {
	var err error
	user.FeedList, err = copyServer.GetUserFeedDeepCopy(user.Id_user)
	if err != nil {
		log.Printf("error in retrving user feed for %s: %v, // function : GetUserFeedDeepCopy", user.Id_user, err)
		return err
	}

	user.userDataInServer, err = copyServer.GetUserDataDeepCopy(user.Id_user)

	if err != nil {
		log.Printf("error in retrving user feed for %s: %v, // function : GetUserDataDeepCopy", user.Id_user, err)
		return err
	}
	return nil
}

func (user *UserAgentUser) Deliberate() {
	user.behaviorStrategy.Deliberate(user)
}

func (ua *UserAgentUser) Act(realServer *srv.ServerAgent) {
	if ua.userDataInServer.Opinion < 0 || ua.userDataInServer.Opinion > 1 {
		fmt.Println("Opinion is not in [0,1] and it's equal to ", ua.userDataInServer.Opinion)
	}
	fmt.Println(ua.Id_user, ua.CurrentDecision.ActionName)
	switch ua.CurrentDecision.ActionName {
	case "Like":
		//fmt.Println(ua.CurrentDecision.ActionName)
		var req types.New_like
		req.Id_user = ua.Id_user
		req.Id_post = ua.CurrentDecision.PostID
		realServer.AddLike(req)
	case "RemoveLike":
		var req types.New_like
		req.Id_user = ua.Id_user
		req.Id_post = ua.CurrentDecision.PostID
		realServer.RemoveLike(req)
	case "Post":
		var req types.New_post
		req.Id_user = ua.Id_user
		ua.GetRandomPost(ua.userDataInServer.Opinion)
		req.Content = ua.CurrentDecision.Content
		realServer.CreatePost(req)
	case "Comment":
		var req types.New_comment
		req.Id_user = ua.Id_user
		req.Id_post = ua.CurrentDecision.PostID
		ua.GetRandomComment(ua.userDataInServer.Opinion)
		req.Content = ua.CurrentDecision.Content
		realServer.CreateComment(req)
	case "Share":
		var req types.Request_sharepost
		req.Id_user = ua.Id_user
		req.Id_post = ua.CurrentDecision.PostID
		realServer.Sharepost(req)
	case "Follow":
		var req types.New_follow
		req.Id_user = ua.Id_user
		req.Id_user_followed = ua.CurrentDecision.OtherUserID
		realServer.Follow(req)
	case "Unfollow":
		var req types.New_follow
		req.Id_user = ua.Id_user
		req.Id_user_followed = ua.CurrentDecision.OtherUserID
		realServer.Unfollow(req)
	case "Nothing":
		return
	}

}

// func (user *UserAgentUser) Deliberate() {

// 	proba_action := rand.Float64()

// 	user.CurrentDecision.ActionName = "Nothing"
// 	if proba_action < user.p_post {
// 		user.CurrentDecision.ActionName = "Post"

// 	} else if proba_action < user.p_post+user.p_like {
// 		// test if feed is empty
// 		if len(user.FeedList) == 0 {
// 			return
// 		}

// 		// find list of posts that are not liked
// 		var list_posts_not_liked []string
// 		for _, post := range user.FeedList {
// 			// test if the key(=post.Id_post) exists in the map
// 			_, findMap := post.List_id_likers[user.Id_user]
// 			if !findMap {
// 				list_posts_not_liked = append(list_posts_not_liked, post.Id_post)
// 			}
// 		}
// 		if len(list_posts_not_liked) == 0 {
// 			return
// 		}

// 		// sort posts by distance

// 		keyValStruct := user.filterPostsByDistanceRanked(list_posts_not_liked, user.PriorOpinion)

// 		if user.typeBehaviour == "rational" {
// 			// for rational agents, we choose the post that is the closest to my opinion
// 			if len(list_posts_not_liked) > 4 {
// 				user.CurrentDecision.PostID = keyValStruct[rand.Intn(4)].Key
// 			} else {
// 				user.CurrentDecision.PostID = keyValStruct[0].Key
// 			}

// 			// for irrational agents, we choose randomly
// 			//////////////////////////////////////////////
// 			//index := rand.Intn(len(list_posts_not_liked))
// 			//user.CurrentDecision.PostID = list_posts_not_liked[index]
// 			//////////////////////////////////////////////
// 		}
// 		if user.typeBehaviour == "troll" {
// 			// for troll agents, we choose the post that is the farthest to my opinion
// 			//////////////////////////////////////////////
// 			if len(list_posts_not_liked) > 4 {
// 				user.CurrentDecision.PostID = keyValStruct[len(keyValStruct)-rand.Intn(4)-1].Key
// 			} else {
// 				user.CurrentDecision.PostID = keyValStruct[len(keyValStruct)-1].Key
// 			}
// 			//////////////////////////////////////////////
// 		}

// 		// for irrational agents, we choose randomly
// 		if user.typeBehaviour == "irrational" {
// 			index := rand.Intn(len(list_posts_not_liked))
// 			user.CurrentDecision.PostID = list_posts_not_liked[index]
// 		}

// 		user.CurrentDecision.ActionName = "Like"

// 	} else if proba_action < user.p_post+user.p_like+user.p_comment {
// 		// test if feed is empty
// 		if len(user.FeedList) == 0 {
// 			return
// 		}

// 		// find list of posts that I have not commented
// 		var list_posts_not_commented []string
// 		for _, post := range user.FeedList {
// 			commented := false
// 			for _, value := range post.List_comments {
// 				if value.Id_author == user.Id_user {
// 					commented = true
// 					break
// 				}
// 			}
// 			if !commented {
// 				list_posts_not_commented = append(list_posts_not_commented, post.Id_post)
// 			}
// 		}
// 		if len(list_posts_not_commented) == 0 {
// 			return
// 		}

// 		// for rational agents, we choose the post that is the closest to my opinion

// 		// for irrational agents, we choose randomly
// 		index := rand.Intn(len(list_posts_not_commented))
// 		user.CurrentDecision.PostID = list_posts_not_commented[index]

// 		// for troll agents, we choose the post that is the farthest to my opinion

// 		user.CurrentDecision.ActionName = "Comment"

// 	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike {
// 		// test if feed is empty
// 		if len(user.FeedList) == 0 {
// 			return
// 		}

// 		// find list of posts that I have liked
// 		var list_posts_liked []string
// 		for _, post := range user.FeedList {
// 			// test if the key(=post.Id_post) exists in the map
// 			_, findMap := post.List_id_likers[user.Id_user]
// 			if findMap {
// 				list_posts_liked = append(list_posts_liked, post.Id_post)
// 			}
// 		}
// 		if len(list_posts_liked) == 0 {
// 			return
// 		}

// 		// sort posts by distance

// 		keyValStruct := user.filterPostsByDistanceRanked(list_posts_liked, user.PriorOpinion)

// 		// for rational agents, we choose to unlike a post that is furthest to my opinion

// 		if user.typeBehaviour == "rational" {
// 			user.CurrentDecision.PostID = keyValStruct[len(keyValStruct)-1].Key
// 		}

// 		if user.typeBehaviour == "troll" {
// 			// for troll agents, we choose to unlike a post that is closest to my opinion
// 			user.CurrentDecision.PostID = keyValStruct[0].Key
// 		}

// 		if user.typeBehaviour == "irrational" {
// 			// for irrational agents, we choose randomly
// 			index := rand.Intn(len(list_posts_liked))
// 			user.CurrentDecision.PostID = list_posts_liked[index]
// 		}

// 		// for irrational agents, we choose randomly
// 		//index := rand.Intn(len(list_posts_liked))
// 		//user.CurrentDecision.PostID = list_posts_liked[index]

// 		// for troll agents, we choose to unlike a post that is closest to my opinion
// 		//user.CurrentDecision.PostID = keyValStruct[0].Key

// 		user.CurrentDecision.ActionName = "RemoveLike"

// 	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow {
// 		// test if feed is empty
// 		if len(user.FeedList) == 0 {
// 			return
// 		}
// 		// find a list of users that I have not followed

// 		var list_users_not_followed_withPriorOpinion []KeyValue

// 		for _, post := range user.FeedList {
// 			_, didIFollow := user.userDataInServer.List_Following[post.Id_author]
// 			if !didIFollow && post.Id_author != user.Id_user {
// 				list_users_not_followed_withPriorOpinion = append(list_users_not_followed_withPriorOpinion, KeyValue{post.Id_author, post.PriorOpinion})
// 			}
// 		}
// 		if len(list_users_not_followed_withPriorOpinion) == 0 {
// 			return
// 		}

// 		// sort following users by distance
// 		keyValStruct := user.filterFollowingByDistanceRanked(list_users_not_followed_withPriorOpinion, user.PriorOpinion)

// 		if user.typeBehaviour == "rational" {
// 			// for rational agents, we choose to follow a user that is closest to my opinion
// 			user.CurrentDecision.OtherUserID = keyValStruct[0].Key
// 		}

// 		if user.typeBehaviour == "troll" {
// 			// for troll agents, we choose to follow a user that is furthest to my opinion
// 			user.CurrentDecision.OtherUserID = keyValStruct[len(keyValStruct)-1].Key
// 		}

// 		if user.typeBehaviour == "irrational" {
// 			// for irrational agents, we choose randomly
// 			index := rand.Intn(len(list_users_not_followed_withPriorOpinion))
// 			user.CurrentDecision.OtherUserID = list_users_not_followed_withPriorOpinion[index].Key
// 		}

// 		// for rational agents, we choose to follow a user that is closest to my opinion
// 		//user.CurrentDecision.OtherUserID = keyValStruct[0].Key
// 		//fmt.Println("I " + user.Id_user + " have just followed" + user.CurrentDecision.OtherUserID)
// 		// for irrational agents, we choose randomly
// 		//index := rand.Intn(len(list_users_not_followed))
// 		//user.CurrentDecision.OtherUserID = list_users_not_followed[index]

// 		// for troll agents, we choose to follow a user that is furthest to my opinion
// 		//user.CurrentDecision.OtherUserID = keyValStruct[len(keyValStruct)-1].Key

// 		user.CurrentDecision.ActionName = "Follow"

// 	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow+user.p_unfollow {
// 		// test if feed is empty
// 		if len(user.FeedList) == 0 {
// 			return
// 		}
// 		// find a list of users that I have followed
// 		var list_users_not_followed_withPriorOpinion []KeyValue
// 		for _, post := range user.FeedList {
// 			_, didIFollow := user.userDataInServer.List_Following[post.Id_author]
// 			if didIFollow {
// 				list_users_not_followed_withPriorOpinion = append(list_users_not_followed_withPriorOpinion, KeyValue{post.Id_author, post.PriorOpinion})
// 			}
// 		}
// 		if len(list_users_not_followed_withPriorOpinion) == 0 {
// 			return
// 		}

// 		// sort following users by distance
// 		keyValStruct := user.filterFollowingByDistanceRanked(list_users_not_followed_withPriorOpinion, user.PriorOpinion)

// 		if user.typeBehaviour == "rational" {
// 			// for rational agents, we choose to unfollow a user that is furthest to my opinion
// 			user.CurrentDecision.OtherUserID = keyValStruct[len(keyValStruct)-1].Key
// 		}

// 		if user.typeBehaviour == "troll" {
// 			// for troll agents, we choose to unfollow a user that is closest to my opinion
// 			user.CurrentDecision.OtherUserID = keyValStruct[0].Key
// 		}

// 		if user.typeBehaviour == "irrational" {
// 			// for irrational agents, we choose randomly
// 			index := rand.Intn(len(list_users_not_followed_withPriorOpinion))
// 			user.CurrentDecision.OtherUserID = list_users_not_followed_withPriorOpinion[index].Key
// 		}
// 		user.CurrentDecision.ActionName = "Unfollow"

// 	} else if proba_action < user.p_post+user.p_like+user.p_comment+user.p_unlike+user.p_follow+user.p_unfollow+user.p_share {
// 		// test if feed is empty
// 		if len(user.FeedList) == 0 {
// 			return
// 		}
// 		// find a list of posts that I have not shared
// 		var list_posts_not_shared []string
// 		for _, post := range user.FeedList {
// 			_, didIShare := user.userDataInServer.List_sharedpost[post.Id_post]
// 			if !didIShare && post.Id_author != user.Id_user {
// 				list_posts_not_shared = append(list_posts_not_shared, post.Id_post)
// 			}
// 		}

// 		if len(list_posts_not_shared) == 0 {
// 			return
// 		}

// 		// sort posts by distance

// 		keyValStruct := user.filterPostsByDistanceRanked(list_posts_not_shared, user.PriorOpinion)

// 		if user.typeBehaviour == "rational" {
// 			// for rational agents, we choose to share a post that is closest to my opinion
// 			user.CurrentDecision.PostID = keyValStruct[0].Key
// 		}

// 		if user.typeBehaviour == "troll" {
// 			// for troll agents, we choose to share a post that is furthest to my opinion
// 			user.CurrentDecision.PostID = keyValStruct[len(keyValStruct)-1].Key
// 		}

// 		if user.typeBehaviour == "irrational" {
// 			index := rand.Intn(len(list_posts_not_shared))
// 			user.CurrentDecision.PostID = list_posts_not_shared[index]
// 		}

// 		user.CurrentDecision.ActionName = "Share"

// 	} else {

// 		// in case the probabilities are not well defined (sum != 1)

// 		// if len(user.FeedList) == 0 {
// 		// 	user.CurrentDecision.ActionName = "Post"
// 		// 	user.CurrentDecision.Content = "content"
// 		// }
// 		fmt.Println("Are u stupid? the sum of probabilities is not equal to 1")

// 	}
// }
