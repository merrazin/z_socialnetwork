package ZSocialNetwork

import (
	types "ZSocialNetwork/types"
)


/*=============     Front End (Social Graph) SECTION     =============================================*/

func (server *ServerAgent) GraphData() types.GraphData {
	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()

	var graphData types.GraphData
	graphData.List_IdUser = []types.GraphDataUser{}
	for _, user := range server.User_map.UserMap {
		var graphDataUser types.GraphDataUser
		graphDataUser.Id_User = user.Id_user
		graphDataUser.Opinion = user.Opinion
		graphDataUser.List_Followers = []string{}
		graphDataUser.List_Following = []string{}
		for follower := range user.List_Followers {
			graphDataUser.List_Followers = append(graphDataUser.List_Followers, follower)
		}
		for following := range user.List_Following {
			graphDataUser.List_Following = append(graphDataUser.List_Following, following)
		}
		graphData.List_IdUser = append(graphData.List_IdUser, graphDataUser)
	}
	return graphData
}

func (server *ServerAgent) GetUserFULLDataFrontEnd(user_id string) types.UserFullData { // à continuer
	var userFullData types.UserFullData

	// créer des deep copies des posts
	feedCopy, _ := server.GetUserFeedDeepCopy(user_id)
	userFullData.Feed = feedCopy

	userFullData.Id_user = user_id

	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()
	userFullData.Opinion = server.User_map.UserMap[user_id].Opinion
	userFullData.Pseudo = server.User_map.UserMap[user_id].Pseudo
	userFullData.PriorOpinion = server.User_map.UserMap[user_id].PriorOpinion

	// copier les listes
	var list_followers []string
	for follower := range server.User_map.UserMap[user_id].List_Followers {
		list_followers = append(list_followers, follower)
	}
	userFullData.List_Followers = list_followers

	var list_following []string
	for following := range server.User_map.UserMap[user_id].List_Following {
		list_following = append(list_following, following)
	}
	userFullData.List_Following = list_following

	var list_ownpost []string
	for ownpost := range server.User_map.UserMap[user_id].List_ownpost {
		list_ownpost = append(list_ownpost, ownpost)
	}
	userFullData.List_ownpost = list_ownpost

	var list_sharedpost []string
	for sharedpost := range server.User_map.UserMap[user_id].List_sharedpost {
		list_sharedpost = append(list_sharedpost, sharedpost)
	}
	userFullData.List_sharedpost = list_sharedpost

	return userFullData

}

