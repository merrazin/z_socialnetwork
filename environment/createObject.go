package ZSocialNetwork

import (
	"errors"
	"fmt"
	"math"
	"strconv"
	"time"

	types "ZSocialNetwork/types"
)

func (server *ServerAgent) CreateUser(newUser types.New_user) error {

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	if _, exists := server.User_map.UserMap[newUser.Id_user]; exists {
		fmt.Println("this username is already taken")
		return errors.New("This username is already taken")
	}

	if newUser.PriorOpinion < 0 || newUser.PriorOpinion > 1 {
		fmt.Println("the prior opinion must be between 0 and 1")
		return errors.New("The prior opinion must be between 0 and 1")
	}
	// initial estimation of the opinion of the user (TO BE DISCUSSED)
	initial_estimated_opinion_basedONChoice := randFloat64InRange(newUser.PriorOpinion+0.3, newUser.PriorOpinion-0.3)

	//initial_estimated_opinion_basedONChoice := rand.Float64()

	// create the user
	server.User_map.UserMap[newUser.Id_user] = types.UserAgent{Id_user: newUser.Id_user,
		Pseudo: newUser.Pseudo, Opinion: initial_estimated_opinion_basedONChoice, PriorOpinion: newUser.PriorOpinion, List_ownpost: make(map[string]bool), List_sharedpost: make(map[string]bool), Feed: make(map[string]bool), List_Followers: make(map[string]bool), List_Following: make(map[string]bool)}
	server.UserCounter++
	return nil
}

/*===========================================================================================================*/

func (server *ServerAgent) CreatePost(req types.New_post) error {

	if req.Content == "" {
		fmt.Println("SERVER : Post content is empty.")
		return errors.New(" SERVER : Post content is empty.")
	}

	if req.Id_user == "" {
		fmt.Println("SERVER : The id_user is empty.")
		return errors.New("SERVER : The id_user is empty.")
	}

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	server.Post_map.PostMu.Lock()
	defer server.Post_map.PostMu.Unlock()

	if _, exists := server.User_map.UserMap[req.Id_user]; !exists {
		fmt.Println("this user doesn't exist.")
		return errors.New("This user doesn't exist.")
	}

	// test if the post already exists

	if _, exists := server.Post_map.PostMap["post"+strconv.Itoa(server.PostCounter)]; exists {
		fmt.Println("This post already exists.")
		return errors.New("This post already exists.")
	}

	currentTime := time.Now().Format(time.RFC3339)
	var resp_id_post string
	resp_id_post = "post" + strconv.Itoa(server.PostCounter)

	server.Post_map.PostMap[resp_id_post] = types.Post{Id_post: resp_id_post,
		Id_author: req.Id_user, Content: req.Content, Date_post: currentTime,
		Opinion: server.User_map.UserMap[req.Id_user].Opinion, PriorOpinion: server.User_map.UserMap[req.Id_user].PriorOpinion, Counter_likes: 0,
		List_id_likers: make(map[string]bool), Counter_comments: 0,
		List_comments: make(map[string]types.CommentData), Counter_shares: 0, List_id_sharers: make(map[string]bool)}
	server.User_map.UserMap[req.Id_user].List_ownpost[resp_id_post] = true
	server.User_map.UserMap[req.Id_user].Feed[resp_id_post] = true
	server.PostCounter++
	return nil
}

/*===========================================================================================================*/

// req types.New_comment
func (server *ServerAgent) CreateComment(req types.New_comment) error {

	if req.Id_post == "" {
		fmt.Println("The id_post is empty.")
		return errors.New("The id_post is empty.")
	}

	if req.Content == "" {
		fmt.Println("SERVER : Comment content is empty.")
		return errors.New("SERVER : Comment content is empty.")
	}

	if req.Id_user == "" {
		fmt.Println("The id_user is empty.")
		return errors.New("The id_user is empty.")
	}

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	server.Post_map.PostMu.Lock()
	defer server.Post_map.PostMu.Unlock()

	if _, exists := server.Post_map.PostMap[req.Id_post]; !exists {
		fmt.Println("this post doesn't exist.")
		return errors.New("This id_post doesn't exist.")
	}

	if _, exists := server.User_map.UserMap[req.Id_user]; !exists {
		fmt.Println("this user doesn't exist.")
		return errors.New("This user doesn't exist.")
	}
	currentTime := time.Now().Format(time.RFC3339)
	Id_comment := "comment" + strconv.Itoa(server.Post_map.PostMap[req.Id_post].Counter_comments)
	newCom := types.CommentData{Id_comment: Id_comment, Id_author: req.Id_user, Content: req.Content, Date_comment: currentTime}

	updatePost := server.Post_map.PostMap[req.Id_post]
	updatePost.List_comments[Id_comment] = newCom
	updatePost.Counter_comments++
	server.Post_map.PostMap[req.Id_post] = updatePost
	//server.Post_map.PostMap[req.Id_post].List_comments[Id_comment] = newCom // problem !!! (either create a new post to update the post or use pointers !)
	//server.Post_map.PostMap[req.Id_post].Counter_comments += 1              // problem !!!

	// Explication de l'opinion:
	// si il y a un grand écart entre l'opinion de l'auteur du post et l'opinion de l'auteur du commentaire, nous ne pourrions pas considérer que le commentaire est pertinent
	// car il y a peu de chance que l'auteur du post et l'auteur du commentaire aient les mêmes opinions
	distance := server.Post_map.PostMap[req.Id_post].Opinion - server.User_map.UserMap[req.Id_user].Opinion

	if server.p_approach_distance_NoChangecomment > math.Abs(float64(distance)) {

		//server.User_map.UserMap[req.Id_user].Opinion = server.User_map.UserMap[req.Id_user].Opinion + 0.1*distance
		updateUser := server.User_map.UserMap[req.Id_user]
		updateUser.Opinion = updateUser.Opinion + server.p_approach_distance_comment*distance
		if updateUser.Opinion < 0 {
			updateUser.Opinion = 0
		} else if updateUser.Opinion > 1 {
			updateUser.Opinion = 1
		}
		server.User_map.UserMap[req.Id_user] = updateUser
	}

	return nil
}