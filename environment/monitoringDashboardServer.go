package ZSocialNetwork

import (
	"math"
	"sort"
	"time"

	types "ZSocialNetwork/types"
)

/*===========================================================================================================*/
/*=============     MONITORING DASHBOARD SECTION     =============================================*/
func (server *ServerAgent) meanAbsoluteError() float64 {
	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()

	var sum float64
	for _, user := range server.User_map.UserMap {
		sum += math.Abs(user.Opinion - user.PriorOpinion)
	}
	mae := sum / float64(len(server.User_map.UserMap))
	mae = math.Round(mae*1000) / 1000
	return mae
}

func (server *ServerAgent) generateTopInfluencers() types.TopInfluencers {
	var topInfluencers types.TopInfluencers

	List_users := []types.UserAgent{}

	server.User_map.UserMu.RLock()

	for _, user := range server.User_map.UserMap {
		List_users = append(List_users, user)
	}
	server.User_map.UserMu.RUnlock()

	sort.Slice(List_users, func(i, j int) bool {
		return len(List_users[i].List_Followers) > len(List_users[j].List_Followers)
	})

	if len(List_users) > 10 {
		List_users = List_users[:10]
	}

	for _, user := range List_users {
		topInfluencers.IdUser_List = append(topInfluencers.IdUser_List, user.Id_user)
		topInfluencers.Opinion_List = append(topInfluencers.Opinion_List, user.Opinion)
		topInfluencers.Count_Followers = append(topInfluencers.Count_Followers, len(user.List_Followers))
	}

	return topInfluencers
}

func (server *ServerAgent) MonitoringData(timeStart time.Time) types.MonitoringData {

	var monitoringData types.MonitoringData

	monitoringData.Top_Influencers = server.generateTopInfluencers()
	monitoringData.MAE_Opinion = server.meanAbsoluteError()
	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()

	//server.Post_map.PostMu.RLock()
	//defer server.Post_map.PostMu.RUnlock()

	//var monitoringData types.MonitoringData
	monitoringData.TimeSimu = time.Since(timeStart).Round(time.Second).String()
	monitoringData.Count_posts = len(server.Post_map.PostMap)

	monitoringData.Count_users = len(server.User_map.UserMap)
	list_opinions_estimated := []float64{}
	for _, user := range server.User_map.UserMap {
		list_opinions_estimated = append(list_opinions_estimated, user.Opinion)
	}
	monitoringData.AllOpinions_Histogram = list_opinions_estimated

	list_opinions_prior := []float64{}
	for _, user := range server.User_map.UserMap {
		list_opinions_prior = append(list_opinions_prior, user.PriorOpinion)
	}
	monitoringData.AllOpinions_Prior = list_opinions_prior

	return monitoringData
}