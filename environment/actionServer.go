package ZSocialNetwork

import (
	"errors"
	"fmt"
	types "ZSocialNetwork/types"
)

/*===========================================================================================================*/

func (server *ServerAgent) AddLike(req types.New_like) error {

	if req.Id_post == "" {
		fmt.Println("The id_post is empty.")
		return errors.New("The id_post is empty.")
	}

	if req.Id_user == "" {
		fmt.Println("The id_user is empty.")
		return errors.New("The id_user is empty.")
	}

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	server.Post_map.PostMu.Lock()
	defer server.Post_map.PostMu.Unlock()

	post, postExists := server.Post_map.PostMap[req.Id_post]
	user, userExists := server.User_map.UserMap[req.Id_user]

	if !postExists {
		fmt.Println("this post doesn't exist.")
		return errors.New("This id_post doesn't exist.")
	}

	if !userExists {
		fmt.Println("this user doesn't exist.")
		return errors.New("This user doesn't exist.")
	}
	// test if the user has already liked the post

	if _, liked := post.List_id_likers[req.Id_user]; liked {
		fmt.Println("This user has already liked this post.")
		return errors.New("This user has already liked this post.")
	}

	distance := post.Opinion - user.Opinion
	user.Opinion = user.Opinion + server.p_approach_distance_like*distance

	if user.Opinion < 0 {
		user.Opinion = 0
	} else if user.Opinion > 1 {
		user.Opinion = 1
	}

	post.Counter_likes++
	post.List_id_likers[req.Id_user] = true

	server.User_map.UserMap[req.Id_user] = user
	server.Post_map.PostMap[req.Id_post] = post

	return nil

}

func (server *ServerAgent) RemoveLike(req types.New_like) error { // Pareil pour cette fonction
	if req.Id_post == "" {
		fmt.Println("the id_post is empty. RemoveLike")
		return errors.New("The id_post is empty.")
	}

	if req.Id_user == "" {
		fmt.Println("the id_user is empty. RemoveLike")
		return errors.New("The id_user is empty.")
	}

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	server.Post_map.PostMu.Lock()
	defer server.Post_map.PostMu.Unlock()

	// Check if the post exists
	currentPost, postExists := server.Post_map.PostMap[req.Id_post]
	if !postExists {
		fmt.Println("this post doesn't exist., RemoveLike ")
		return errors.New("This id_post doesn't exist.")
	}

	// Check if the user exists
	currentUser, userExists := server.User_map.UserMap[req.Id_user]
	if !userExists {
		fmt.Println("this user doesn't exist., RemoveLike ")
		return errors.New("This user doesn't exist.")
	}

	// Check if the user has liked the post
	if _, liked := currentPost.List_id_likers[req.Id_user]; !liked {
		fmt.Println("This user didn't like this post., RemoveLike ")
		return errors.New("This user didn't like this post.")
	}

	// distance := currentPost.Opinion - currentUser.Opinion
	// currentUser.Opinion = currentUser.Opinion - server.p_approach_distance_like*distance

	// if currentUser.Opinion < 0 {
	// 	currentUser.Opinion = 0
	// } else if currentUser.Opinion > 1 {
	// 	currentUser.Opinion = 1
	// }

	currentPost.Counter_likes--

	delete(currentPost.List_id_likers, req.Id_user)

	server.User_map.UserMap[req.Id_user] = currentUser
	server.Post_map.PostMap[req.Id_post] = currentPost

	return nil
}

/*===========================================================================================================*/

func (server *ServerAgent) Follow(req types.New_follow) error {
	if req.Id_user == "" {
		fmt.Println("the id_user is empty. Follow")
		return errors.New("The id_user is empty.")
	}

	if req.Id_user_followed == "" {
		fmt.Println("the id_user_followed is empty. Follow")
		return errors.New("The id_user is empty.")
	}

	if req.Id_user == req.Id_user_followed {
		fmt.Println("One user can't follow himself., Follow")
		return errors.New("One user can't follow himself.")
	}

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	currentUser, userExists := server.User_map.UserMap[req.Id_user]
	if !userExists {
		fmt.Println("this user doesn't exist., Follow")
		return errors.New("This user doesn't exist.")
	}

	if req.Id_user_followed == "" {
		fmt.Println("the id_user_followed is empty., Follow")
		return errors.New("The id_user_followed is empty.")
	}

	currentUserFollowed, userFollowedExists := server.User_map.UserMap[req.Id_user_followed]
	if !userFollowedExists {
		fmt.Println("this future user followed doesn't exist., Follow")
		return errors.New("This future user followed doesn't exist.")
	}

	if req.Id_user == req.Id_user_followed {
		fmt.Println("One user can't follow himself., Follow")
		return errors.New("One user can't follow himself.")
	}

	// Check if the user is already following the followed user
	if _, alreadyFollowing := currentUser.List_Following[req.Id_user_followed]; alreadyFollowing {
		fmt.Println("This user is already following this user., Follow")
		return errors.New("This user is already following this user.")
	}

	// Check if the followed user is already followed by the user
	if _, alreadyFollowed := currentUserFollowed.List_Followers[req.Id_user]; alreadyFollowed {
		fmt.Println("This user is already followed by this user., Follow")
		return errors.New("This user is already followed by this user.")
	}

	distance := currentUserFollowed.Opinion - currentUser.Opinion
	currentUser.Opinion = currentUser.Opinion + server.p_approach_distance_follow*distance

	if currentUser.Opinion < 0 {
		currentUser.Opinion = 0
	} else if currentUser.Opinion > 1 {
		currentUser.Opinion = 1
	}

	currentUser.List_Following[req.Id_user_followed] = true
	currentUserFollowed.List_Followers[req.Id_user] = true

	server.User_map.UserMap[req.Id_user] = currentUser
	server.User_map.UserMap[req.Id_user_followed] = currentUserFollowed
	//fmt.Println("the server : "+req.Id_user+"just followed the user", req.Id_user_followed)
	return nil
}

/*===========================================================================================================*/
func (server *ServerAgent) Unfollow(req types.New_follow) error {
	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	if req.Id_user == "" {
		fmt.Println("the id_user is empty. Unfollow")
		return errors.New("The id_user is empty.")
	}

	currentUser, userExists := server.User_map.UserMap[req.Id_user]
	if !userExists {
		fmt.Println("this user doesn't exist., Unfollow")
		return errors.New("This user doesn't exist.")
	}

	if req.Id_user_followed == "" {
		fmt.Println("the id_user_followed is empty., Unfollow")
		return errors.New("The id_user_followed is empty.")
	}

	currentUserFollowed, userFollowedExists := server.User_map.UserMap[req.Id_user_followed]
	if !userFollowedExists {
		fmt.Println("this future user to unfollowed doesn't exist., Unfollow")
		return errors.New("This future user to unfollowed doesn't exist.")
	}

	if req.Id_user == req.Id_user_followed {
		fmt.Println("One user can't unfollow himself., Unfollow")
		return errors.New("One user can't unfollow himself.")
	}

	// Find and remove the followed user from List_Following

	if _, alreadyFollowing := currentUser.List_Following[req.Id_user_followed]; !alreadyFollowing {
		fmt.Println("This user doesn't follow this user in the first place., Unfollow")
		return errors.New("This user doesn't follow this user in the first place.")
	}

	// Find and remove the follower from List_Followers

	if _, alreadyFollowed := currentUserFollowed.List_Followers[req.Id_user]; !alreadyFollowed {
		fmt.Println("This user doesn't follow this user in the first place., Unfollow")
		return errors.New("This user doesn't follow this user in the first placec.")
	}

	if returnVal, alreadyFollowing := currentUser.List_Following[req.Id_user_followed]; alreadyFollowing {
		if returnVal == true {
			delete(currentUser.List_Following, req.Id_user_followed)
		}
	}

	if returnVal, alreadyFollowed := currentUserFollowed.List_Followers[req.Id_user]; alreadyFollowed {
		if returnVal == true {
			delete(currentUserFollowed.List_Followers, req.Id_user)
		}
	}

	// Update opinions
	// distance := currentUserFollowed.Opinion - currentUser.Opinion
	// currentUser.Opinion = currentUser.Opinion - server.p_approach_distance_follow*distance

	// if currentUser.Opinion < 0 {
	// 	currentUser.Opinion = 0
	// } else if currentUser.Opinion > 1 {
	// 	currentUser.Opinion = 1
	// }
	server.User_map.UserMap[req.Id_user] = currentUser
	server.User_map.UserMap[req.Id_user_followed] = currentUserFollowed

	//fmt.Println("the server : "+req.Id_user+"just UNfollowed the user", req.Id_user_followed)

	return nil
}

/*===========================================================================================================*/

func (server *ServerAgent) Sharepost(req types.Request_sharepost) error {

	if req.Id_post == "" {
		fmt.Println("the server : ERROR IN  "+req.Id_user+" sharing the post, The id_post is empty.", req.Id_post)
		return errors.New("The id_post is empty.")
	}

	if req.Id_user == "" {
		fmt.Println("the server : ERROR IN  "+req.Id_user+" sharing the post, The id_user is empty.", req.Id_post)
		return errors.New("The id_user is empty.")
	}

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	server.Post_map.PostMu.Lock()
	defer server.Post_map.PostMu.Unlock()

	currentPost, postExists := server.Post_map.PostMap[req.Id_post]
	if !postExists {
		fmt.Println("the server : ERROR IN  "+req.Id_user+" sharing the post, This id_post doesn't exist ", req.Id_post)
		return errors.New("This id_post doesn't exist.")
	}

	currentUser, userExists := server.User_map.UserMap[req.Id_user]
	if !userExists {
		fmt.Println("the server : ERROR IN  "+req.Id_user+" sharing the post, This user doesn't exist.", req.Id_post)
		return errors.New("This user doesn't exist.")
	}

	if req.Id_user == currentPost.Id_author {
		fmt.Println("the server : ERROR IN  "+req.Id_user+" sharing the post, A user can't share his own post.", req.Id_post)
		return errors.New("A user can't share his own post.")
	}

	// Check if the post has already been shared by the user
	if _, alreadyShared := currentUser.List_sharedpost[currentPost.Id_post]; alreadyShared {
		fmt.Println("the server : ERROR IN  "+req.Id_user+" sharing the post, This user has already shared this post. 1", req.Id_post)
		return errors.New("This user has already shared this post.")
	}

	// Check if the user has already shared the post
	if _, alreadyShared := currentPost.List_id_sharers[currentUser.Id_user]; alreadyShared {
		fmt.Println("the server : ERROR IN  "+req.Id_user+" sharing the post, This user has already shared this post. 2", req.Id_post)
		return errors.New("This user has already shared this post.")
	}

	// Update user and post data
	currentUser.List_sharedpost[currentPost.Id_post] = true
	currentPost.List_id_sharers[currentUser.Id_user] = true
	currentPost.Counter_shares++

	distance := currentPost.Opinion - currentUser.Opinion
	currentUser.Opinion = currentUser.Opinion + server.p_approach_distance_share*distance

	if currentUser.Opinion < 0 {
		currentUser.Opinion = 0
	} else if currentUser.Opinion > 1 {
		currentUser.Opinion = 1
	}

	// if 0.4 > math.Abs(float64(distance)) {
	// 	currentUser.Opinion = currentUser.Opinion + server.p_approach_distance_share*distance
	// 	/*if currentUser.Opinion < 0{
	// 		currentUser.Opinion = 0

	// 	}else if currentUser.Opinion > 1{
	// 		currentUser.Opinion = 1

	// 	}*/
	// }

	// add the post to the feed of the user

	currentUser.Feed[currentPost.Id_post] = true

	server.User_map.UserMap[req.Id_user] = currentUser
	server.Post_map.PostMap[req.Id_post] = currentPost

	//fmt.Println("the server : "+req.Id_user+"just shared the post", req.Id_post)

	return nil
}