package ZSocialNetwork

func removeDuplicatesList(input []string) []string {
	uniqueMap := make(map[string]bool)
	result := []string{}

	for _, str := range input {
		if !uniqueMap[str] {
			result = append(result, str)
			uniqueMap[str] = true
		}
	}

	return result
}

func subtractLists(list1, list2 []string) []string {
	// Vérifier que les listes ne sont pas vides.
	if len(list1) == 0 || len(list2) == 0 {
		return list1
	}

	// Create a map to store elements from list2
	list2Elements := make(map[string]bool)
	for _, item := range list2 {
		list2Elements[item] = true
	}

	// Create the result list by checking elements from list1
	var result []string
	for _, item := range list1 {
		if !list2Elements[item] {
			result = append(result, item)
		}
	}

	return result
}

func MapContainsStr(tableHachage map[string]bool, str string) bool {
	for key := range tableHachage {
		if key == str {
			return true
		}
	}
	return false
}

func maxi(a, b float64) float64 {
	if a > b {
		return a
	}
	return b
}
func mini(a, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

/*================================== userAgent.go ===========================================================*/

func (Server *ServerAgent) ChooseUserByOpinion(opinion float64) string {
	Server.RWMu.RLock()
	defer Server.RWMu.RUnlock()
	min := maxi(0, opinion-0.2)
	max := mini(1.0, opinion+0.2)

	for _, user := range Server.User_map.UserMap {
		if user.Opinion >= min && user.Opinion <= max {
			return user.Id_user
		}
	}
	return ""
}

/*==========================================================================*/