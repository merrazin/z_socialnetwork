package ZSocialNetwork

import (
	"errors"
	"fmt"
	"math"
	"math/rand"
	"sort"
	"sync"

	types "ZSocialNetwork/types"
)

// Pour ces deux structures , on peut directement les intégrer dans ServerAgent.

type UserMapLock struct {
	UserMap map[string]types.UserAgent
	UserMu  sync.RWMutex
}

type PostMapLock struct {
	PostMap map[string]types.Post
	PostMu  sync.RWMutex
}

type ServerAgent struct {
	RWMu        sync.RWMutex
	Id          string
	User_map    UserMapLock
	UserCounter int
	Post_map    PostMapLock
	PostCounter int

	// PARAMETERS TO BE TUNED
	p_alpha                             float64 // distance between the opinion of the user and the opinion of the post to consider the post as relevant
	p_maxPosts                          int     // maximum number of posts to add to the feed of the user when updating the feed with relevant posts
	p_approach_distance_like            float64
	p_approach_distance_follow          float64
	p_approach_distance_share           float64
	p_approach_distance_comment         float64
	p_approach_distance_NoChangecomment float64
	p_relevance_counterLike_coef        int
	p_relevance_counterComments_coef    int
	p_relevance_counterShare_coef       int
}

func NewServerAgent(id_server string) *ServerAgent {

	// PARAMETERS TO BE TUNED
	p_alpha := 0.2
	p_maxPosts := 10

	p_approach_distance_like := 0.5
	p_approach_distance_follow := 0.5
	p_approach_distance_share := 0.5
	p_approach_distance_comment := 0.5
	p_approach_distance_NoChangecomment := 0.4

	p_relevance_counterLike_coef := 5
	p_relevance_counterComments_coef := 2
	p_relevance_counterShare_coef := 10

	return &ServerAgent{Id: id_server, UserCounter: 0, PostCounter: 0,
		User_map: UserMapLock{UserMap: make(map[string]types.UserAgent)},
		Post_map: PostMapLock{PostMap: make(map[string]types.Post)},
		p_alpha:  p_alpha, p_maxPosts: p_maxPosts,
		p_approach_distance_like:            p_approach_distance_like,
		p_approach_distance_follow:          p_approach_distance_follow,
		p_approach_distance_share:           p_approach_distance_share,
		p_approach_distance_comment:         p_approach_distance_comment,
		p_approach_distance_NoChangecomment: p_approach_distance_NoChangecomment,
		p_relevance_counterLike_coef:        p_relevance_counterLike_coef,
		p_relevance_counterComments_coef:    p_relevance_counterComments_coef,
		p_relevance_counterShare_coef:       p_relevance_counterShare_coef}
}
func randFloat64InRange(min, max float64) float64 {
	val := min + rand.Float64()*(max-min)
	if val > 1 {
		return 1
	}
	if val < 0 {
		return 0
	}
	return val
}




/*server.Post_map.PostMu.Lock()
post, postExists := server.Post_map.PostMap[req.Id_post]
server.Post_map.PostMu.Unlock() // Déverrouillez immédiatement après avoir obtenu le post

if !postExists {
    return errors.New("This id_post doesn't exist.")
}

server.User_map.UserMu.Lock()
user, userExists := server.User_map.UserMap[req.Id_user]
server.User_map.UserMu.Unlock() // Déverrouillez immédiatement après avoir obtenu l'utilisateur
if !userExists {
		return errors.New("This user doesn't exist.")
	}
*/
// Je pense dans addLike c'est mieux de faire comme cela au cas ou on a un accès concurrentiel élévé parce que dans ton cas Adnane la fonction verouille simultanément les mutexes pour les utilisateurs et les posts.


// getNewFollowingOriginalPosts cette fonction retourne les posts originaux publiés par les utilisateurs suivis par l'utilisateur passé en paramètre
func (server *ServerAgent) getNewFollowingOriginalPosts(Id_user string) ([]string, error) {
	if Id_user == "" {
		fmt.Println("The id_user is empty. getNewFollowingOriginalPosts")
		return nil, errors.New("The id_user is empty.")
	}

	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()

	currentUser, userExists := server.User_map.UserMap[Id_user]
	if !userExists {
		fmt.Println("This user doesn't exist func getNewFollowingOriginalPosts.")
		return nil, errors.New("This user doesn't exist.")
	}

	list_posts_published := []string{}

	for userFollowingID := range currentUser.List_Following {
		userFollowing, exists := server.User_map.UserMap[userFollowingID]
		if !exists {
			fmt.Println("This user doesn't exist func getNewFollowingOriginalPosts, and this should not happen...")
			continue
		}

		for postID := range userFollowing.List_ownpost {
			list_posts_published = append(list_posts_published, postID)
		}
	}

	currentFeed := []string{}
	for postID := range currentUser.Feed {
		currentFeed = append(currentFeed, postID)
	}

	list_posts_published = subtractLists(list_posts_published, currentFeed)

	return list_posts_published, nil
}

// /*===========================================================================================================*/

// cette fonction retourne les posts partagés par les utilisateurs suivis par l'utilisateur passé en paramètre
func (server *ServerAgent) getNewFollowingSharedPosts(Id_user string) (map[string][]string, error) {
	if Id_user == "" {
		fmt.Println("The id_user is empty. getNewFollowingSharedPosts")
		return nil, errors.New("The id_user is empty.")
	}

	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()

	currentUser, userExists := server.User_map.UserMap[Id_user]
	if !userExists {
		fmt.Println("This user doesn't exist func getNewFollowingSharedPosts.")
		return nil, errors.New("This user doesn't exist.")
	}

	list_posts_shared := make(map[string][]string)

	for userFollowingID := range currentUser.List_Following {
		userFollowing, exists := server.User_map.UserMap[userFollowingID]
		if !exists {
			fmt.Println("This user doesn't exist func getNewFollowingSharedPosts, and this should not happen...")
			continue
		}

		for postID := range userFollowing.List_sharedpost {
			list_posts_shared[userFollowingID] = append(list_posts_shared[userFollowingID], postID)
		}
	}

	currentFeed := []string{}
	for postID := range currentUser.Feed {
		currentFeed = append(currentFeed, postID)
	}

	for userFollowingID := range list_posts_shared {
		list_posts_shared[userFollowingID] = subtractLists(list_posts_shared[userFollowingID], currentFeed)
	}

	return list_posts_shared, nil
}

/*===========================================================================================================*/
// getGlobalTrendingPosts

// cette fonction retourne les posts les plus tendances dans le réseau social

func (server *ServerAgent) getGlobalTrendingPosts() []types.TrendPosts {
	server.Post_map.PostMu.RLock()         // R LOCK CAN BE USED HERE
	defer server.Post_map.PostMu.RUnlock() // R LOCK CAN BE USED HERE

	var list_posts_tend []types.TrendPosts

	for _, post := range server.Post_map.PostMap {
		var trendpost types.TrendPosts
		trendpost.Id_post = post.Id_post
		trendpost.Opinion = post.Opinion
		trendpost.Score = post.Counter_likes*server.p_relevance_counterLike_coef + post.Counter_comments*server.p_relevance_counterComments_coef + post.Counter_shares*server.p_relevance_counterShare_coef
		list_posts_tend = append(list_posts_tend, trendpost)
	}

	// Sort the list in descending order of score
	sort.Slice(list_posts_tend, func(i, j int) bool {
		return list_posts_tend[i].Score > list_posts_tend[j].Score
	})

	return list_posts_tend
}

// getReleventPosts à faire à partir de l'opinion de l'utilisateur et le retour de getGlobalTrendingPosts
// nous retournons les posts qui ont une opinion proche par alpha distancede celle de l'utilisateur ET QUI ne sont pas déjà dans le feed de l'utilisateur


// cette fonction retourne les posts les plus tendances dans le réseau social et qui sont pertinents pour l'utilisateur passé en paramètre (c'est à dire les posts qui ont une opinion proche de celle de l'utilisateur et qui ne sont pas déjà dans le feed de l'utilisateur)
func (server *ServerAgent) getReleventPosts(user_id string) ([]types.TrendPosts, error) {

	// Get the global trending posts
	list_posts_trending := server.getGlobalTrendingPosts()

	server.User_map.UserMu.RLock() /////////////////if Rlock is used, it MIGHT CAUSE A DEAD LOCK !!! : solution ? use regular lock in getReleventPosts and getGlobalTrendingPosts
	defer server.User_map.UserMu.RUnlock()

	user, exists := server.User_map.UserMap[user_id]
	if !exists {
		fmt.Println("This user doesn't exist func getReleventPosts.")
		return nil, errors.New("user not found")
	}
	var list_posts_relevent []types.TrendPosts
	counterPosts := 0

	// Filter relevant posts
	for _, post := range list_posts_trending {
		if math.Abs(float64(post.Opinion-user.Opinion)) < float64(server.p_alpha) && !MapContainsStr(user.Feed, post.Id_post) && counterPosts < server.p_maxPosts {
			list_posts_relevent = append(list_posts_relevent, post)
			counterPosts++
		}
	}

	return list_posts_relevent, nil
}

/*===========================================================================================================*/
// cette fonction ajoute les posts les plus tendances dans le réseau social (résultat de getReleventPosts) au feed de l'utilisateur passé en paramètre

func (server *ServerAgent) updateFeedTrendyPosts(user_id string) error {

	// Get the relevant posts
	list_posts_relevent, err := server.getReleventPosts(user_id)

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	user, exists := server.User_map.UserMap[user_id]
	if !exists {
		fmt.Println("This user doesn't exist func updateFeedTrendyPosts.")
		return errors.New("user not found")
	}

	if err != nil {
		return err
	}

	for _, post := range list_posts_relevent {
		user.Feed[post.Id_post] = true
	}
	return nil
}

/*===========================================================================================================*/

/*===========================================================================================================*/
// alpha is the distance between the opinion of the user and the opinion of the post
// cette fonction met à jour le feed de l'utilisateur passé en paramètre en ajoutant les posts les plus tendances dans le réseau social (résultat de getReleventPosts) et les posts originaux publiés par les utilisateurs suivis par l'utilisateur et les posts partagés par les utilisateurs suivis par l'utilisateur
func (server *ServerAgent) UpdateUserFeed(user_id string) error {

	// Get the new posts published by the users followed by the user
	list_posts_published, err := server.getNewFollowingOriginalPosts(user_id)
	if err != nil {
		return err
	}

	// Get the new posts shared by the users followed by the user

	list_posts_shared, err := server.getNewFollowingSharedPosts(user_id)
	if err != nil {
		return err
	}

	// Get the relevant posts

	list_posts_relevent, err := server.getReleventPosts(user_id)
	if err != nil {
		return err
	}

	// Update the feed

	server.User_map.UserMu.Lock()
	defer server.User_map.UserMu.Unlock()

	user, _ := server.User_map.UserMap[user_id]

	for _, postID := range list_posts_published {
		user.Feed[postID] = true
	}

	for _, list_posts_shared_by_user := range list_posts_shared {
		for _, postID := range list_posts_shared_by_user {
			user.Feed[postID] = true
		}
	}

	for _, post := range list_posts_relevent {
		user.Feed[post.Id_post] = true
	}

	return nil
}

/* problem*/
/*===========================================================================================================*/

func (server *ServerAgent) GetUserFeedDeepCopy(user_id string) ([]types.Post, error) {
	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()

	user, exists := server.User_map.UserMap[user_id]
	if !exists {
		fmt.Println("This user doesn't exist func GetUserFeedDeepCopy.")
		return nil, errors.New("user not found")
	}

	server.Post_map.PostMu.RLock()
	defer server.Post_map.PostMu.RUnlock()

	var feed []types.Post
	for postID := range user.Feed {
		post, exists := server.Post_map.PostMap[postID]
		if !exists {
			fmt.Println("This post doesn't exist --ERR in func GetUserFeedDeepCopy-- and this should not happen...")
			continue
		}
		// create deep copy of the post (we don't want to return a pointer to the post in the server)
		copyPost := types.Post{
			Id_post:          post.Id_post,
			Id_author:        post.Id_author,
			Content:          post.Content,
			Date_post:        post.Date_post,
			Opinion:          post.Opinion,
			PriorOpinion:     post.PriorOpinion,
			Counter_likes:    post.Counter_likes,
			List_id_likers:   make(map[string]bool),
			Counter_comments: post.Counter_comments,
			List_comments:    make(map[string]types.CommentData),
			Counter_shares:   post.Counter_shares,
			List_id_sharers:  make(map[string]bool),
		}

		// Copy the maps
		for key, value := range post.List_id_likers {
			copyPost.List_id_likers[key] = value
		}

		for key, value := range post.List_comments {
			copyPost.List_comments[key] = value
		}

		for key, value := range post.List_id_sharers {
			copyPost.List_id_sharers[key] = value
		}

		feed = append(feed, copyPost)
	}

	return feed, nil
}

func (server *ServerAgent) GetUserDataDeepCopy(user_id string) (types.UserAgent, error) {
	server.User_map.UserMu.RLock()
	defer server.User_map.UserMu.RUnlock()

	user, exists := server.User_map.UserMap[user_id]
	if !exists {
		fmt.Println("This user doesn't exist func GetUserDataDeepCopy.")
		return types.UserAgent{}, errors.New("user not found")
	}

	// Create a deep copy of the UserAgent
	copyUser := types.UserAgent{
		Id_user:         user.Id_user,
		Pseudo:          user.Pseudo,
		Opinion:         user.Opinion,
		PriorOpinion:    user.PriorOpinion,
		List_ownpost:    make(map[string]bool),
		List_sharedpost: make(map[string]bool),
		Feed:            make(map[string]bool),
		List_Followers:  make(map[string]bool),
		List_Following:  make(map[string]bool),
	}

	// Copy the maps
	for key, value := range user.List_ownpost {
		copyUser.List_ownpost[key] = value
	}

	for key, value := range user.List_sharedpost {
		copyUser.List_sharedpost[key] = value
	}

	for key, value := range user.Feed {
		copyUser.Feed[key] = value
	}

	for key, value := range user.List_Followers {
		copyUser.List_Followers[key] = value
	}

	for key, value := range user.List_Following {
		copyUser.List_Following[key] = value
	}

	return copyUser, nil
}

// func (server *ServerAgent) LoadPostsFromFile(filePath string) error {

// 	file, err := os.Open(filePath)

// 	if err != nil {

// 		return err

// 	}

// 	defer file.Close()

// 	scanner := bufio.NewScanner(file)

// 	postId := 1 // ou une autre logique pour générer les IDs

// 	for scanner.Scan() {

// 		line := scanner.Text()

// 		parts := strings.Split(line, ";")

// 		if len(parts) != 2 {

// 			continue

// 		}

// 		content := strings.TrimSpace(parts[0])

// 		opinionStr := strings.Replace(parts[1], ",", ".", -1)

// 		opinion, err := strconv.ParseFloat(strings.TrimSpace(opinionStr), 64)

// 		if err != nil {

// 			continue

// 		}

// 		// Créer et ajouter le post dans PostMap

// 		server.Post_map.PostMu.Lock()

// 		server.Post_map.PostMap[strconv.Itoa(postId)] = types.Post{

// 			Id_post: strconv.Itoa(postId),

// 			Content: content,

// 			Opinion: opinion,
// 		}

// 		server.Post_map.PostMu.Unlock()

// 		postId++

// 	}

// 	if err := scanner.Err(); err != nil {

// 		return err

// 	}

// 	return nil

// }

// /*===========================================================================================================*/
func (server *ServerAgent) Start() {

}

/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/
/*===========================================================================================================*/

// func (server *ServerAgent) GetUserDataCopy(user_id string) (types.UserAgent, error) {
// 	server.User_map.UserMu.RLock()
// 	defer server.User_map.UserMu.RUnlock()
// 	user, exists := server.User_map.UserMap[user_id]
// 	if !exists {
// 		return types.UserAgent{}, errors.New("user not found")
// 	}
// 	return user, nil
// }

// func (server *ServerAgent) GetUserDataDeepCopy(user_id string) (types.UserAgent, error) {
// 	server.User_map.UserMu.RLock()
// 	defer server.User_map.UserMu.RUnlock()

// 	user, exists := server.User_map.UserMap[user_id]
// 	if !exists {
// 		return types.UserAgent{}, errors.New("user not found")
// 	}

// 	// Create a deep copy of the UserAgent
// 	copyUser := types.UserAgent{
// 		Id_user:         user.Id_user,
// 		Pseudo:          user.Pseudo,
// 		Opinion:         user.Opinion,
// 		PriorOpinion:    user.PriorOpinion,
// 		List_ownpost:    make(map[string]bool),
// 		List_sharedpost: make(map[string]bool),
// 		Feed:            make(map[string]bool),
// 		List_Followers:  make(map[string]bool),
// 		List_Following:  make(map[string]bool),
// 	}

// 	// Copy the maps
// 	for key, value := range user.List_ownpost {
// 		copyUser.List_ownpost[key] = value
// 	}

// 	for key, value := range user.List_sharedpost {
// 		copyUser.List_sharedpost[key] = value
// 	}

// 	for key, value := range user.Feed {
// 		copyUser.Feed[key] = value
// 	}

// 	for key, value := range user.List_Followers {
// 		copyUser.List_Followers[key] = value
// 	}

// 	for key, value := range user.List_Following {
// 		copyUser.List_Following[key] = value
// 	}

// 	return copyUser, nil
// }

// func (server *ServerAgent) GetAllUsersDataDeepCopy() ([]types.UserAgent, error) {
// 	server.User_map.UserMu.RLock()
// 	defer server.User_map.UserMu.RUnlock()

// 	// Create a slice to store deep copies of all users
// 	allUsers := make([]types.UserAgent, 0, len(server.User_map.UserMap))

// 	// Iterate through all users in the UserMap
// 	for _, user := range server.User_map.UserMap {
// 		// Create a deep copy of the UserAgent
// 		copyUser := types.UserAgent{
// 			Id_user:         user.Id_user,
// 			Pseudo:          user.Pseudo,
// 			Opinion:         user.Opinion,
// 			PriorOpinion:    user.PriorOpinion,
// 			List_ownpost:    make(map[string]bool),
// 			List_sharedpost: make(map[string]bool),
// 			Feed:            make(map[string]bool),
// 			List_Followers:  make(map[string]bool),
// 			List_Following:  make(map[string]bool),
// 		}

// 		// Copy the maps
// 		for key, value := range user.List_ownpost {
// 			copyUser.List_ownpost[key] = value
// 		}

// 		for key, value := range user.List_sharedpost {
// 			copyUser.List_sharedpost[key] = value
// 		}

// 		for key, value := range user.Feed {
// 			copyUser.Feed[key] = value
// 		}

// 		for key, value := range user.List_Followers {
// 			copyUser.List_Followers[key] = value
// 		}

// 		for key, value := range user.List_Following {
// 			copyUser.List_Following[key] = value
// 		}

// 		// Append the deep copy to the slice
// 		allUsers = append(allUsers, copyUser)
// 	}

// 	return allUsers, nil
// }

// /*===========================================================================================================*/
// func (server *ServerAgent) showFeedSafeCopy(user_id string) ([]types.Post, error) {
// 	server.User_map.UserMu.RLock()
// 	defer server.User_map.UserMu.RUnlock()

// 	user, exists := server.User_map.UserMap[user_id]
// 	if !exists {
// 		return nil, errors.New("user not found")
// 	}

// 	var list_posts []types.Post

// 	for postID := range user.Feed {
// 		post, exists := server.Post_map.PostMap[postID]
// 		if !exists {
// 			fmt.Println("This post doesn't exist func showFeed, and this should not happen...")
// 			continue
// 		}
// 		list_posts = append(list_posts, post)
// 	}

// 	// Sort the list in descending order of date
// 	sort.Slice(list_posts, func(i, j int) bool {
// 		return list_posts[i].Date_post > list_posts[j].Date_post
// 	})

// 	var list_posts_safe_copy []types.Post
// 	for _, post := range list_posts {
// 		list_posts_safe_copy = append(list_posts_safe_copy, post)
// 	}

// 	return list_posts_safe_copy, nil
// }

// THIS IS THE CORRECT WAY TO DO IT. THE ABOVE CODE IS JUST FOR DEBUGGING PURPOSES
// var list_posts_safeCopy []types.Post

// for postID := range user.Feed {
// 	post, exists := server.Post_map.PostMap[postID]
// 	if !exists {
// 		fmt.Println("This post doesn't exist func showFeed, and this should not happen...")
// 		continue
// 	}

// 	// Create a copy of the post to prevent modification of original maps
// 	copyPost := types.Post{
// 		Id_post:          post.Id_post,
// 		Id_author:        post.Id_author,
// 		Content:          post.Content,
// 		Date_post:        post.Date_post,
// 		Counter_likes:    post.Counter_likes,
// 		List_id_likers:   make(map[string]bool),
// 		Counter_comments: post.Counter_comments,
// 		List_comments:    make(map[string]types.CommentData),
// 		Counter_shares:   post.Counter_shares,
// 		List_id_sharers:  make(map[string]bool),
// 		Opinion:          post.Opinion,
// 	}

// 	// Copy values from the original post to the copy
// 	for liker := range post.List_id_likers {
// 		copyPost.List_id_likers[liker] = true
// 	}

// 	for commenter, commentData := range post.List_comments {
// 		copyPost.List_comments[commenter] = commentData
// 	}

// 	for sharer := range post.List_id_sharers {
// 		copyPost.List_id_sharers[sharer] = true
// 	}

// 	// Append the copy to the result slice
// 	list_posts_safeCopy = append(list_posts_safeCopy, copyPost)
// }

// return list_posts_safeCopy, nil

// brouillon

// cette fonc

// func (server *ServerAgent) updateFeedNewFollowingOriginalPosts(user_id string) error {

// 	// Get the new posts published by the users followed by the user // might create a deadlock if we use Rlock here
// 	list_posts_published, err := server.getNewFollowingOriginalPosts(types.Request_getNewFollowingOriginalSharedPosts{Id_user: user_id})
// 	if err != nil {
// 		return err
// 	}

// 	server.User_map.UserMu.Lock()
// 	defer server.User_map.UserMu.Unlock()

// 	user, exists := server.User_map.UserMap[user_id]
// 	if !exists {
// 		return errors.New("user not found")
// 	}

// 	for _, postID := range list_posts_published {
// 		user.Feed[postID] = true
// 	}

// 	return nil
// }

// /*===========================================================================================================*/

// func (server *ServerAgent) updateFeedNewFollowingSharedPosts(user_id string) error {

// 	// Get the new posts shared by the users followed by the user

// 	list_posts_shared, err := server.getNewFollowingSharedPosts(types.Request_getNewFollowingOriginalSharedPosts{Id_user: user_id})
// 	if err != nil {
// 		return err
// 	}

// 	server.User_map.UserMu.Lock()
// 	defer server.User_map.UserMu.Unlock()

// 	user, exists := server.User_map.UserMap[user_id]
// 	if !exists {
// 		return errors.New("user not found")
// 	}
// 	for _, list_posts_shared_by_user := range list_posts_shared {
// 		for _, postID := range list_posts_shared_by_user {
// 			user.Feed[postID] = true
// 		}
// 	}

// 	return nil
// }
